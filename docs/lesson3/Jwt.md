# [MasterClass WebApi .NET Core - Autentification et Autorisation](README.md)  

## Json Web Token  

### Principe  

un token JWT se présente sous la forme {header}.{payload}.{signature}

- Le header est utilisé pour décrire le jeton
- le payload contient les informations
- la signature permet de s'assurer de la fiabilité de ces informations

Le header et le payload sont des objets json encodé en base64.
La signature est un hash de {header}.{payload} à partir d'une clé

Tout client ayant connaissance de la clé est donc capable de vérifier la validité de la signature. De plus le payload étant un json en base64, tout le monde est capable de récupérer son contenu  
La sécurité de cette solution vient du fait que si le header ou le payload sont modifiés la signature n'est plus valide.  
Il est également possible d'utiliser un algorithme de chiffrement asymétrique afin de fournir aux applicatifs une clé publique qui permet seulement de valider la signature et de garder secrète une clé privée qui permet de générer la signature.


L'authentification JWT a plusieurs avantages :
- La légéreté de la solution car aucun token n'est persisté  
- Un gain de performance car le client n'a pas besoin d'appeler le serveur pour récupérer les informations stockées dans le token (par contre attention à ne pas mettre de données sensibles dedans car tout le monde peut les lire)
- Une solution robuste

Par contre l'inconvénient majeure de cette solution est qu'il est impossible de révoquer un token avant la fin de sa validité sans perdre la légéreté de la solution

### Implémentation  

Nous allons partir sur un algorythme symétrique pour notre API car notre clé de chiffrement n'aura pas à être partagée (la génération et la validation du token seront faits systématiquement au niveau de notre API)

Ajout d'une classe d'option
```c#
public class JwtOptions
{
    public bool Enabled { get; set; }
    public string Issuer { get; set; }
    public string Key { get; set; }
    public TimeSpan Duration { get; set; }
}
```

et d'une entrée dans le *appSettings.json*
```json
"JwtOptions": {
  "Enabled": true,
  "Issuer": "soat.fr",
  "Key": "xa4Z9HqY%}s/v~F>",
  "Duration": "0.00:30:00"
}
```

On crée une méthode d'extension afin d'ajouter le schéma d'authentification et le middleware nécessaire pour valider l'authentification
```c#
public static IServiceCollection AddMasterClassJwt(this IServiceCollection services, IConfiguration config)
{
    var jwtConfigSection = config.GetSection(nameof(JwtOptions));
    services.Configure<JwtOptions>(jwtConfigSection);
    
    var jwtOptions = jwtConfigSection.Get<JwtOptions>();

    if (jwtOptions?.Enabled ?? false)
    {
        services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = jwtOptions.Issuer,
                    IssuerSigningKey = new SymmetricSecurityKey(jwtOptions.Key.ToBytes()),
                    AuthenticationType = JwtBearerDefaults.AuthenticationScheme,
                    ClockSkew = TimeSpan.FromSeconds(0)
                };
            });
    }

    return services;
}
```

On ajoute le package System.IdentityModel.Tokens.Jwt à notre couche de service afin de pouvoir y générer le token  
> dotnet add ./src/MasterClass.Service/MasterClass.Service.csproj package System.IdentityModel.Tokens.Jwt

On crée une méthode d'extension pour gérer la création de nos claims  
```c#
private const string JWTROLE_CLAIMNAME = "role";

public static IEnumerable<Claim> GetJwtClaims(this User user, DateTimeOffset issuedAt)
{
    foreach (var role in user.Roles)
    {
        yield return new Claim(JWTROLE_CLAIMNAME, role);
    }

    yield return new Claim(JwtRegisteredClaimNames.Iat, issuedAt.ToUnixTimeSeconds().ToString());
    yield return new Claim(JwtRegisteredClaimNames.UniqueName, user.Id.ToString());
    yield return new Claim(JwtRegisteredClaimNames.Sub, user.Login);
    yield return new Claim(JwtRegisteredClaimNames.GivenName, user.Name);
    yield return new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString());
}
```

On ajoute un token au modèle exposé en sortie du *authenticate*
```c#
using MasterClass.Repository.Users.Models;
using MasterClass.Service.Users.Contracts.Models;

namespace MasterClass.Service.Users.Models
{
    public class AuthenticatedUser : IAuthenticatedUser
    {
        public int Id { get; }

        public string Token { get; }

        private AuthenticatedUser(int id, string token)
        {
            Id = id;
            Token = token;
        }

        internal static AuthenticatedUser Create(User user, string token)
            => user == null ? null : new AuthenticatedUser(user.Id, token);
    }
}
```

On génère notre token dans notre couche de service en s'appuyant sur notre option (passée dans le constructeur par injection de dépendance)  
```c#
public IAuthenticatedUser Authenticate(AuthenticateParameters authParams, DateTimeOffset issuedAt)
{
    var user = _userBusiness.AuthenticateUser(authParams.Login, authParams.Password);
    if (user != null)
    {
        var jwtToken = _options.Enabled
            ? new JwtSecurityToken(
                issuer: _options.Issuer,
                claims: user.GetJwtClaims(issuedAt),
                expires: issuedAt.LocalDateTime.Add(_options.Duration),
                signingCredentials: new SigningCredentials(new SymmetricSecurityKey(_options.Key.ToBytes()), SecurityAlgorithms.HmacSha256))
            : null;

        return AuthenticatedUser.Create(user, jwtToken == null ? null : new JwtSecurityTokenHandler().WriteToken(jwtToken));
    }
    return null;
}
```
Dans la classe *Startup* on appelle notre méthode d'extensions  
```c#
services.AddMasterClassJwt(Configuration);
```
Et on positionne le middleware d'authentification. Tous les middleware déclarés avant ne pourront pas s'appuyer sur l'authentification mais ceux d'après auront l'information.
```c#
app.UseAuthentication();
```

Enfin on fait en sorte d'avoir une route authentifiée  
```c#
using MasterClass.Service.Users.Contracts;
using MasterClass.Service.Users.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MasterClass.WebApi.Controllers
{
    [Route("api/user"), Authorize]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly ISystemClock _clock;

        public UserController(IUserService userService, ISystemClock clock)
        {
            _userService = userService;
            _clock = clock;
        }

        [HttpGet]
        public IActionResult GetContext() => Ok(new { Id = User.Identity.Name });

        [HttpPost, Route("authenticate"), AllowAnonymous]
        public IActionResult Authenticate([FromBody]AuthenticateParameters authParams)
        {
            var authUser = _userService.Authenticate(authParams, _clock.UtcNow);
            return authUser == null ? (IActionResult)Unauthorized() : Ok(authUser);
        }
    }
}
```

Dans cet exemple nous avons authentifié toutes les routes du controlleur par défaut, et autorisé explicitement la route *authenticate* à ne pas l'être. Nous aurions pu faire l'inverse  
Nous nous basons également sur *ISystemClock* qui permet de récupérer l'heure (et de la mocker éventuellement en cas de test)

### Header d'authentification dans Swagger  

Nous allons rajouter de l'authentification sur notre swagger afin de pouvoir appeler les méthodes authentifiées
```c#
services.AddSwaggerGen(genOptions =>
    {
        genOptions.SwaggerDoc(SWAGGER_DOCNAME,
            new Swashbuckle.AspNetCore.Swagger.Info
            {
                Title = "MasterClass WebApi",
                Version = VERSION
            });
        genOptions.AddJwtBearerSecurity();
    });
```
```c#
private static void AddJwtBearerSecurity(this SwaggerGenOptions options)
{
    options.AddSecurityDefinition(
        JwtBearerDefaults.AuthenticationScheme,
        new ApiKeyScheme
        {
            Name = "Authorization",
            In = "header"
        });
    options.AddSecurityRequirement(
        new Dictionary<string, IEnumerable<string>>
        {
            { JwtBearerDefaults.AuthenticationScheme, new string[] {} }
        });
}
```

[Page suivante : Cookie d'authentification](Cookie.md)  
[Retour au menu](README.md)