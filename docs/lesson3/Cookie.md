# [MasterClass WebApi .NET Core - Autentification et Autorisation](README.md)  

## Cookie d'authentification

Nous allons gérer un deuxième mode d'authentification dans notre application.

Ajout d'une méthode *SignIn* dans notre couche de service qui va permettre de renvoyer un *ClaimsPrincipal*
```c#
public ClaimsPrincipal SignIn(AuthenticateParameters authParams, string scheme)
{
    var user = _userBusiness.AuthenticateUser(authParams.Login, authParams.Password);
    return user == null ? null : user.GetClaimsPrincipal(scheme);
}
```
```c#
public static ClaimsPrincipal GetClaimsPrincipal(this User user, string scheme)
{
    var identity = new GenericIdentity(user.Id.ToString(), scheme);
    identity.AddClaim(new Claim(ClaimTypes.GivenName, user.Name));
    identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Login));
    return new GenericPrincipal(identity, user.Roles);
}
```

Ajout d'une route *signin* dans UserController qui va permettre de créer le cookie et d'une route *signout* qui va permettre de le supprimer
```c#
[HttpPost("signin"), AllowAnonymous]
public async Task<IActionResult> SignInAsync([FromBody]AuthenticateParameters authParams)
{
    var principal = _userService.SignIn(authParams, CookieAuthenticationDefaults.AuthenticationScheme);
    if (principal != null)
    {
        await HttpContext.SignInAsync(principal, new AuthenticationProperties { IsPersistent = true });
        return Ok();
    }
    return Unauthorized();
}

[HttpPost("signout")]
public async Task<IActionResult> SignOutAsync()
{
    await HttpContext.SignOutAsync();
    return Ok();
}
```

Création d'un classe de settings pour gérer nos cookies
```c#
public class CookieOptions
{
    public bool Enabled { get; set; }
    public string Issuer { get; set; }
    public string Name { get; set; }
    public TimeSpan Duration { get; set; }
}
```

Et configuration de ces settings dans le fichier *appsettings.json*
```json
"CookieOptions": {
  "Enabled": true,
  "Issuer": "localhost",
  "Name": "MasterClass.Cookie",
  "Duration": "0.00:30:00"
}
```

Modifions notre classe d'extensions de manière à n'avoir qu'une méthode publique pour l'ajout de notre système d'authentification.  
Dans .Net Core la policy par défaut pour l'autorisation ne valide que le schéma par défaut (et ne gère donc pas le multi schéma). Afin de ne pas avoir à rajouter l'ensemble de nos schémas au niveau de l'attribut *Authorize* nous allons en profiter pour changer la policy par défaut  
```c#
using MasterClass.Core.Options;
using MasterClass.Core.Tools;
using MasterClass.WebApi.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace MasterClass.WebApi.StartupExtensions
{
    public static class AuthenticationExtensions
    {
        public static IServiceCollection AddMasterClassAuthentication(this IServiceCollection services, IConfiguration config)
        {
            var authBuilder = new AuthorizationPolicyBuilder();
            
            if (services.AddMasterClassJwt(config, JwtBearerDefaults.AuthenticationScheme))
            {
                authBuilder.AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme);
            }

            if (services.AddMasterClassCookie(config, CookieAuthenticationDefaults.AuthenticationScheme))
            {
                authBuilder.AddAuthenticationSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
            }

            var defaultPolicy = authBuilder
                .RequireAuthenticatedUser()
                .Build();

            services.AddAuthorization(options => options.DefaultPolicy = defaultPolicy);

            return services;
        }

        private static bool AddMasterClassCookie(this IServiceCollection services, IConfiguration config, string scheme)
        {
            var cookieOptions = config.GetSection(nameof(CookieOptions)).Get<CookieOptions>();
            if (cookieOptions?.Enabled ?? false)
            {
                services
                    .AddAuthentication(options
                        => options.DefaultSignInScheme
                            = options.DefaultSignOutScheme
                            = scheme)
                    .AddCookie(options =>
                    {
                        options.Cookie.Name = cookieOptions.Name;
                        options.Cookie.Domain = cookieOptions.Issuer;
                        options.EventsType = typeof(WebApiCookieAuthenticationEvents);
                        options.ExpireTimeSpan = cookieOptions.Duration;
                    });

                services.AddScoped<WebApiCookieAuthenticationEvents>();

                return true;
            }

            return false;
        }

        private static bool AddMasterClassJwt(this IServiceCollection services, IConfiguration config, string scheme)
        {
            var jwtConfigSection = config.GetSection(nameof(JwtOptions));
            services.Configure<JwtOptions>(jwtConfigSection);

            var jwtOptions = jwtConfigSection.Get<JwtOptions>();

            if (jwtOptions?.Enabled ?? false)
            {
                services
                    .AddAuthentication(scheme)
                    .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateAudience = false,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = jwtOptions.Issuer,
                            IssuerSigningKey = new SymmetricSecurityKey(jwtOptions.Key.ToBytes()),
                            AuthenticationType = scheme,
                            ClockSkew = TimeSpan.FromSeconds(0)
                        };
                    });

                    return true;
            }

            return false;
        }
    }
}
```

Nous créons une classe *WebApiCookieAuthenticationEvents* afin de surcharger les comportements de redirection standards de l'authent par cookie.  
```c#
public class WebApiCookieAuthenticationEvents : CookieAuthenticationEvents
{
    public override async Task RedirectToAccessDenied(RedirectContext<CookieAuthenticationOptions> context)
    {
        await ReturnStatus(context.Response, StatusCodes.Status403Forbidden);
    }

    public async override Task RedirectToLogin(RedirectContext<CookieAuthenticationOptions> context)
    {
        await ReturnStatus(context.Response, StatusCodes.Status401Unauthorized);
    }

    public async override Task RedirectToLogout(RedirectContext<CookieAuthenticationOptions> context)
    {
        await ReturnStatus(context.Response, StatusCodes.Status401Unauthorized);
    }

    public async override Task RedirectToReturnUrl(RedirectContext<CookieAuthenticationOptions> context)
    {
        await ReturnStatus(context.Response, StatusCodes.Status401Unauthorized);
    }

    private async Task ReturnStatus(HttpResponse response, int status)
    {
        response.StatusCode = status;
        await Task.CompletedTask;
    }
}
```

*AddMasterClassAuthentication* permet :
- De rajouter l'authent JWT si elle est activée  
- De rajouter l'authent cookie si elle est activée  
- De créer un *Policy* par défaut de manière à n'avoir aucun argument à passer à notre attribut *Authorize* (de base il n'y a qu'un seul schéma d'authent par défaut)  

Nous l'appelons dans la classe de *Startup*
```c#
services.AddMasterClassAuthentication(Configuration);
```

[Page suivante : Autorisation](Authorization.md)  
[Retour au menu](README.md)