# MasterClass WebApi .NET Core - Autentification et Autorisation

## [Json Web Token](Jwt.md#json-web-token) (30min)  
### [Principe](Jwt.md#principe)  
### [Implémentation](Jwt.md#implémentation)  
### [Header d'authentification dans Swagger](Jwt.md#header-dauthentification-dans-swagger)  

## [Cookie d'authentification](Cookie.md#cookie-dauthentification) (30min)  

## [Autorisation](Authorization.md#autorisation) (40min)  
### [Role](Authorization.md#role)  
### [Policy](Authorization.md#policy)  
### [Requirement](Authorization.md#requirement)  
### [Policy Provider](Authorization.md#policy-provider)  