# [MasterClass WebApi .NET Core - Autentification et Autorisation](README.md)  

## Autorisation  

### Role  

Il est possible de restreindre l'accès à des actions ou des controlleurs suivants les rôles de l'utilisateur  
```c#
[Route("api/admin"), Authorize, Authorize(Roles = "SuperAdmin, Admin")]
public class AdminController : Controller
{
    [HttpGet]
    public IActionResult Index()
    {
        return Ok();
    }
    
    [HttpGet("grant"), Authorize(Roles = "SuperAdmin")]
    public IActionResult GrantAccess()
    {
        return Ok();
    }
}
```

Dans cet exemple nous cumulons les attributs authorize. Celui sans paramètre s'assure que l'utilisateur est authentifié via la policy par default. Le deuxième vérifie que l'utilisateur a un role suffisant pour y accèder en s'appuyant sur les claims de l'utilisateur authentifié  

### Policy

Nous pouvons regrouper un certainement nombre de prérequis sous forme d'une Policy. Nous allons par exemple regrouper la gestion des roles au seins d'une Policy.  
```c#
services.AddAuthorization(options =>
{
    options.DefaultPolicy = defaultPolicy;
    options.AddPolicy(Policies.REQUIRED_SUPERADMIN_ROLE, policy => policy.RequireRole("SuperAdmin"));
    options.AddPolicy(Policies.REQUIRED_ADMIN_ROLE, policy => policy.RequireRole("SuperAdmin, Admin"));
});
```

Notre controlleur devient
```c#
[Route("api/admin"), Authorize, Authorize(Policy = Policies.REQUIRED_ADMIN_ROLE)]
public class AdminController : Controller
{
    [HttpGet]
    public IActionResult Index()
    {
        return Ok();
    }
    
    [HttpGet("grant"), Authorize(Policy = Policies.REQUIRED_SUPERADMIN_ROLE)]
    public IActionResult GrantAccess()
    {
        return Ok();
    }
}
```

Nous allons faire évoluer la définition d'un *SuperAdmin*.  
Tel que cela a été fait jusque maintenant un *SuperAdmin* était un role à part entière.  
A partir de maintenant un *SuperAdmin* est un *Admin* avec un droit de *SuperAdmin*.

Nous allons faire évoluer notre modèle *User* dans notre couche de repository de manière à rajouter cette notion de droit
```c#
public class User
{
    // ...
    public string[] Rights { get; set; }
}
```

Nous ajoutons maintenant l'information dans nos *Claims*
```c#
public static IEnumerable<Claim> GetJwtClaims(this User user, DateTimeOffset issuedAt)
{
    // ...
    foreach (var rightClaim in user.GetMasterClassClaims())
    {
        yield return rightClaim;
    }
    // ...
}

public static ClaimsPrincipal GetClaimsPrincipal(this User user, string scheme)
{
    // ...
    identity.AddClaims(user.GetMasterClassClaims());
    // ...
}

private static IEnumerable<Claim> GetMasterClassClaims(this User user)
{
    return user.Rights == null
        ? Enumerable.Empty<Claim>()
        : user.Rights.Select(right => new Claim(MasterClassClaims.RIGHTS_CLAIMNAME, right));
}
```

Maintenant nous allons faire évoluer notre Policy :
```c#
services.AddAuthorization(options =>
{
    options.DefaultPolicy = defaultPolicy;
    options.AddPolicy(Policies.REQUIRED_SUPERADMIN_ROLE, policy =>
        policy.RequireRole("Admin")
            .RequireClaim(MasterClassClaims.RIGHTS_CLAIMNAME, "SuperAdmin"));
    options.AddPolicy(Policies.REQUIRED_ADMIN_ROLE, policy => policy.RequireRole("Admin"));
});
```

### Requirement  

Nous pouvons avoir besoin de controle d'accès spécifique par exemple de controller qu'un utilisateur a plus de 18 ans pour pouvoir acheter de l'alcool  
Nous allons utiliser les Requirement dans ce but  
```c#
public class MinimumAgeRequirement : IAuthorizationRequirement
{
    public int MinimumAge { get; }

    public MinimumAgeRequirement(int minimumAge)
    {
        MinimumAge = minimumAge;
    }
}
```

Nous ajoutons une Policy avec ce Requirement  
```c#
options.AddPolicy(Policies.REQUIRED_ATLEAST_18, policy =>
    policy.Requirements.Add(new MinimumAgeRequirement(18)));
```

Nous ajoutons une route qui utilise cette policy
```c#
[Route("api/alcohol"), Authorize, Authorize(Policy = Policies.REQUIRED_ATLEAST_18)]
public class AlcoholController : Controller
{
    [HttpGet]
    public IActionResult Index()
    {
        return Ok();
    }
}
```

Nous ajoutons une date de naissance à notre objet *User*
```c#
public class User
{
    // ...
    public DateTime BirthDate { get; set; }
}
```

Nous ajoutons le claim lors de notre authentification
```c#
private static IEnumerable<Claim> GetMasterClassClaims(this User user)
{
    var rightClaims = user.Rights == null
        ? Enumerable.Empty<Claim>()
        : user.Rights.Select(right => new Claim(MasterClassClaims.RIGHTS_CLAIMNAME, right));

    return rightClaims
        .Concat(new [] { new Claim(ClaimTypes.DateOfBirth, user.BirthDate.ToString() ) });
}
```

Nous rajoutons un handler qui va permettre de valider le Requirement
```c#
public class MinimumAgeHandler : AuthorizationHandler<MinimumAgeRequirement>
{
    protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
                                                MinimumAgeRequirement requirement)
    {
        if (!context.User.HasClaim(c => c.Type == ClaimTypes.DateOfBirth))
        {
            return Task.CompletedTask;
        }

        var dateOfBirth = Convert.ToDateTime(
            context.User.FindFirst(c => c.Type == ClaimTypes.DateOfBirth).Value);

        int calculatedAge = DateTime.Today.Year - dateOfBirth.Year;
        if (dateOfBirth > DateTime.Today.AddYears(-calculatedAge))
        {
            calculatedAge--;
        }

        if (calculatedAge >= requirement.MinimumAge)
        {
            context.Succeed(requirement);
        }

        return Task.CompletedTask;
    }
}
```

Et enfin nous enregistrons ce handler  
```c#
services.AddSingleton<IAuthorizationHandler, MinimumAgeHandler>();
```

### Policy Provider

Il peut être intéressant de créer dynamiquement les policy.  
Actuellement le type par défaut utilisé est un *DefaultAuthorizationPolicyProvider* qui se base sur les options fournies dans le *AddAuthorisation*.
Voici ce que fait actuellement le *DefaultAuthorizationPolicyProvider*
```c#
public class DefaultAuthorizationPolicyProvider : IAuthorizationPolicyProvider
{
    private readonly AuthorizationOptions _options;

    public DefaultAuthorizationPolicyProvider(IOptions<AuthorizationOptions> options)
    {
        if (options == null)
        throw new ArgumentNullException(nameof (options));
        this._options = options.Value;
    }

    public Task<AuthorizationPolicy> GetDefaultPolicyAsync()
    {
        return Task.FromResult<AuthorizationPolicy>(this._options.DefaultPolicy);
    }

    public virtual Task<AuthorizationPolicy> GetPolicyAsync(string policyName)
    {
        return Task.FromResult<AuthorizationPolicy>(this._options.GetPolicy(policyName));
    }
}
```

Dans notre cas cela va nous permettre de créer par défaut les schémas utilisés et également de se baser sur une option pour récupérer l'age minimal pour notre controller *Alcohol* (cet age peut varier d'un pays à un autre)

Création d'une factory afin de créer les Policy. Nous nous basons sur le *IAuthenticationSchemeProvider* qui va nous permettre de récupérer tous les schémas enregistrés  
```c#
public class AuthorizationPolicyFactory : IAuthorizationPolicyFactory
{
    private readonly Task<IEnumerable<AuthenticationScheme>> _schemes;
    private readonly AlcoholOptions _alcoholOptions;

    public AuthorizationPolicyFactory(IAuthenticationSchemeProvider schemeProvider, IOptions<AlcoholOptions> alcoholOptions)
    {
        _schemes = schemeProvider.GetAllSchemesAsync();
        _alcoholOptions = alcoholOptions.Value;
    }

    public async Task<AuthorizationPolicy> CreateDefaultAsync()
    {
        return (await CreatePolicyBuilderAsync())
            .RequireAuthenticatedUser()
            .Build();
    }

    public async Task<AuthorizationPolicy> CreateAsync(string policyName)
    {
        switch (policyName)
        {
            case Policies.REQUIRED_SUPERADMIN_ROLE:
                return await CreateSuperAdminPolicyAsync();
            case Policies.REQUIRED_ADMIN_ROLE:
                return await CreateAdminPolicyAsync();
            case Policies.REQUIRED_MINIMUM_AGE_ALCOHOL:
                return await CreateAtLeastPolicyAsync(_alcoholOptions.MinimumAge);
            default:
                throw new ArgumentException($"{policyName} not found", policyName);
        }
    }

    private async Task<AuthorizationPolicy> CreateSuperAdminPolicyAsync()
    {
        var policyBuilder = await CreatePolicyBuilderAsync();
        policyBuilder.RequireRole("Admin")
            .RequireClaim(MasterClassClaims.RIGHTS_CLAIMNAME, "SuperAdmin");
        return policyBuilder.Build();
    }

    private async Task<AuthorizationPolicy> CreateAdminPolicyAsync()
    {
        var policyBuilder = await CreatePolicyBuilderAsync();
        policyBuilder.RequireRole("Admin");
        return policyBuilder.Build();
    }

    private async Task<AuthorizationPolicy> CreateAtLeastPolicyAsync(int minimumAge)
    {
        var policyBuilder = await CreatePolicyBuilderAsync();
        policyBuilder.Requirements.Add(new MinimumAgeRequirement(minimumAge));
        return policyBuilder.Build();
    }

    private async Task<AuthorizationPolicyBuilder> CreatePolicyBuilderAsync()
    {
        return new AuthorizationPolicyBuilder((await GetAllSchemesAsync()).ToArray());
    }

    private async Task<IEnumerable<string>> GetAllSchemesAsync()
    {
        return (await _schemes).Select(scheme => scheme.Name);
    }
}
```

Création de notre provider : nous laissons la possibilité d'utiliser des options comme sur le *DefaultAuthorizationPolicyProvider*  
```c#
public class MasterClassAuthorizationPolicyProvider : IAuthorizationPolicyProvider
{
    private IAuthorizationPolicyFactory _policyFactory;
    private readonly AuthorizationOptions _options;
    private readonly object _lock = new object();

    public MasterClassAuthorizationPolicyProvider(IAuthorizationPolicyFactory policyFactory, IOptions<AuthorizationOptions> options)
    {
        _policyFactory = policyFactory;
        _options = options.Value;
    }

    public async Task<AuthorizationPolicy> GetDefaultPolicyAsync()
    {
        if (_options.DefaultPolicy == null)
        {
            await AddDefaultPolicyAsync();
        }
        return _options.DefaultPolicy;
    }

    public async Task<AuthorizationPolicy> GetPolicyAsync(string policyName)
    {
        var policy = _options.GetPolicy(policyName);
        if (policy == null)
        {
            policy = await AddPolicyAsync(policyName);
        }
        return policy;
    }

    private async Task AddDefaultPolicyAsync()
    {
        var defaultPolicy = await _policyFactory.CreateDefaultAsync();
        lock (_lock)
        {
            if (_options.DefaultPolicy == null)
            {
                _options.DefaultPolicy = defaultPolicy;
            }
        }
    }

    private async Task<AuthorizationPolicy> AddPolicyAsync(string policyName)
    {
        var policy = await _policyFactory.CreateAsync(policyName);
        lock (_lock)
        {
            if (_options.GetPolicy(policyName) == null)
            {
                _options.AddPolicy(policyName, policy);
            }
        }

        return policy;
    }
}
```

On ajoute l'enregistrement de ces 2 classes et de la classe d'options, ainsi que refacto de la méthode *AddMasterClassAuthentication*  
```c#
public static IServiceCollection AddMasterClassAuthentication(this IServiceCollection services, IConfiguration config)
{
    services.AddMasterClassJwt(config);
    services.AddMasterClassCookie(config);

    return services;
}

public static IServiceCollection AddMasterClassPolicy(this IServiceCollection services, IConfiguration config)
{
    services.Configure<AlcoholOptions>(config.GetSection(nameof(AlcoholOptions)));
    services.AddAuthorization(options => options.DefaultPolicy = null);

    services.AddSingleton<IAuthorizationPolicyFactory, AuthorizationPolicyFactory>();
    services.AddTransient<IAuthorizationPolicyProvider, MasterClassAuthorizationPolicyProvider>();
    services.AddSingleton<IAuthorizationHandler, MinimumAgeHandler>();

    return services;
}
```

Dans la classe *Startup*
```c#
services.AddMasterClassAuthentication(Configuration);
services.AddMasterClassPolicy(Configuration);
```

[Retour au menu](README.md)