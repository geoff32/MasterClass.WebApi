# [MasterClass WebApi .NET Core - Introduction](README.md)  

## Settings  

### Settings par défaut  

Un fichier *appsettings.json* est présent dans la solution. Son utilisation est ajouté par défaut dans la construction de notre webhost.
```c#
WebHost.CreateDefaultBuilder(args)
```

Nous allons rajouter un settings afin de rendre configurable le texte renvoyé par notre test de vie.
```json
"Diagnostic": {
  "HealthCheckContent": "system_ok"
}
```

Nous allons mettre nos classes de settings dans un projet Core.
> cd src  
dotnet new classlib -n MasterClass.Core

Ajout du projet dans la solution
> cd ..  
dotnet sln add ./src/MasterClass.Core/MasterClass.Core.csproj

On ajoute la référence de ce projet au projet MasterClass.WebApi
> dotnet add ./src/MasterClass.WebApi/MasterClass.WebApi.csproj reference ./src/MasterClass.Core/MasterClass.Core.csproj

Puis on ajoute la classe *DiagnosticOptions* dans le projet Core
```c#
public class DiagnosticOptions
{
    public string HealthCheckContent { get; set; }
}
```

Nous allons ensuite ajouter un constructeur public à notre *DiagnosticController* et utiliser la propriété *HealthCheckContent* de notre classe de settings
```c#
private readonly DiagnosticOptions _options;

public DiagnosticController(IOptions<DiagnosticOptions> options)
{
    _options = options.Value;
}

[HttpGet, HttpHead, Route("healthcheck")]
public IActionResult HealthCheck() => Ok(_options.HealthCheckContent);
```

Enfin il ne nous reste plus qu'à enregistrer notre classe de settings dans la classe *Startup* afin de pouvoir résoudre la création d'une instance de notre controller via injection de dépendance
```c#
services.Configure<DiagnosticOptions>(Configuration.GetSection("Diagnostic"));
```

Avec cette solution la valeur de notre settings n'est pas prise à chaud : si on modifie le fichier settings il faut redémarrer l'application afin de prendre en compte la modification.

Pour que la valeur du settings soit fixée uniquement sur le cycle de vie de la requête, nous pouvons utiliser *IOptionsSnapshot* (Chaque nouvelle requête récuperera la valeur actuelle du settings et la préservera pendant sa durée de vie)
```c#
public DiagnosticController(IOptionsSnapshot<DiagnosticOptions> options)
```

Pour avoir le changement à chaud, nous pouvons utiliser un *IOptionsMonitor*
```c#
public DiagnosticController(IOptionsMonitor<DiagnosticOptions> options)
{
    _options = options.CurrentValue;
}
```

### Settings par environnement  

Lors de la construction de notre webHost un fichier *appsetings.{environment}.json* est également chargé si il est présent.  
*{environment}* est récupéré à partir de la variable d'environnement *ASPNETCORE_ENVIRONMENT*    
Ce fichier ira se greffer en surcharge du fichier *appsettings.json*  

Nous allons créer un fichier de settings pour la production
> echo. > ./src/MasterClass.WebApi/appsettings.Production.json

```json
{
    "Diagnostic": {
        "HealthCheckContent": "prod_ok"
    }
}
```

Dans le fichier *launch.json* nous pouvons modifier la variable d'environnement afin d'utiliser notre settings :  
```json
"env": {
    "ASPNETCORE_ENVIRONMENT": "Production"
},
```

[Page suivante : Middleware](Middleware.md)  
[Retour au menu](README.md)