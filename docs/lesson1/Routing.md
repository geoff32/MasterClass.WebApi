# [MasterClass WebApi .NET Core - Introduction](README.md)  

## Action et routage  

### Table de routage  

Il existe différentes manières d'enregistrer nos routes dans la table de routage :
- *MapRoute* permet d'associer un template d'url à une action d'un controller. Les templates sont pris dans l'ordre de déclarations au moment de la résolution de l'URL : le premier template qui correspond donnera l'action de controller à utiliser (les autres seront ignorés).  
- Le routage par attribut. En utilisant l'attribut *Route* sur le controller, vous définissez un préfixe pour toutes les actions du controller. En l'utilisant au niveau de l'action vous complétez le prefix de votre controller.  

### Ajout d'une route  

Un test de vie est indispensable afin de permettre par exemple à un loadbalancer de vérifier la bonne santé de l'applicatif. Si le test de vie sur un serveur est KO, le loadbalancer prendra de lui même la décision de sortir la machine du pool

Cela sera un bon exercice afin de créer notre première route  
- Création d'un nouveau controller *DiagnosticController* dans le dossier Controllers
```c#
public class DiagnosticController : Controller
```
- Création de l'action
```c#
public IActionResult HealthCheck() => Ok("system_ok");
```
- Gestion du prefix de routage global au controller
```c#
[Route("api/_system")]
```
- Gestion du routage au niveau de l'action de manière à précier la route, et les méthodes http autorisées
```c#
[HttpGet, HttpHead, Route("healthcheck")]
```

l'url *http://localhost:5000/api/_system/healthcheck* répond alors en GET et en HEAD

[Page suivante : Utilisation du fichier de settings](Settings.md)  
[Retour au menu](README.md)