# [MasterClass WebApi .NET Core - Introduction](README.md)  

## Initialisation du projet  

### Prérequis  

- VS Code
- Docker et Docker Compose
- Extensions C#, optionnelle Docker
- Fiddler ou Postman

### Création du projet  

Dans la console VS Code
Création d'un fichier solution
> dotnet new sln -n MasterClass.WebApi

Création d'un répertoire src
> mkdir src  
cd src

Création d'un projet webapi :
> dotnet new webapi -n MasterClass.WebApi

Ajout du projet dans la solution
> cd ..  
dotnet sln add ./src/MasterClass.WebApi/MasterClass.WebApi.csproj

Build de la solution
> dotnet build

Le plugin C# vous propose d'ajouter les fichiers nécessaires pour le debug  
![](../images/required_assets.png)

[Page suivante : Action et routage](Routing.md)  
[Retour au menu](README.md)