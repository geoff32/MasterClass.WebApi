# [MasterClass WebApi .NET Core - Introduction](README.md)  

## Middleware  

Un middleware est un composant qui s'inscrit dans un pipeline d’application pour gérer les requêtes et les réponses.  
Chaque composant :
- Choisit de passer la requête au composant suivant dans le pipeline.
- Peut travailler avant et après l’appel du composant suivant dans le pipeline.

Nous allons ajouter un middleware qui va nous permettre de tracker le serveur qui traite la requête dans le header de la réponse (pratique sur une ferme de serveur afin d'identifier un serveur défaillant)

```c#
public class TrackMachineMiddleware
{
    private readonly RequestDelegate _next;

    public TrackMachineMiddleware(RequestDelegate next)
    {
        _next = next;
    }
    
    public async Task InvokeAsync(HttpContext context)
    {
        context.Response.Headers.Add("X-Machine", Environment.MachineName);
        await _next(context);
    }
}
```

L'appel au delegate _next permet d'indiquer que nous faisons appel au composant suivant.
Si nous voulions arrêter le traitement il suffirait de ne pas l'appeler. Il est également possible d'effectuer du traitement après cet appel qui s'exécutera donc après les composants suivants dans le pipe.  
Afin d'utiliser ce middleware, nous l'insérons dans le pipeline applicatif de la manière suivante dans la classe *Startup*
```c#
app.UseMiddleware<TrackMachineMiddleware>();
```

Lorsque nous faisons tourner l'application sur un docker, nous pouvons d'ailleurs constater que le nom du conteneur nous est renvoyé dans le header de la réponse  
![](../images/track_machine.png)

[Page suivante : Injection de dépendances](DependencyInjection.md)  
[Retour au menu](README.md)