# MasterClass WebApi .NET Core - Introduction  

## [Initialisation du projet](Initialize.md#initialisation-du-projet) (10min)
### [Prérequis](Initialize.md#prérequis)  
### [Création du projet](Initialize.md#création-du-projet)  

## [Action et routage](Routing.md#action-et-routage) (15min)  
### [Table de routage](Routing.md#table-de-routage)  
### [Ajout d'une route](Routing.md#ajout-dune-route)  

## [Utilisation du fichier de settings](Settings.md#utilisation-du-fichier-de-settings) (20min)  
### [Settings par défaut](Settings.md#settings-par-défaut)  
### [Settings par environnement](Settings.md#settings-par-environnement)

## [Middleware](Middleware.md#middleware) (20min)  

## [Injection de dépendances](DependencyInjection.md#injection-de-dépendances) (45min)  
### [Introduction](DependencyInjection.md#introduction)  
### [Injection de dépendance dans un middleware](DependencyInjection.md#injection-de-dépendance-dans-un-middleware)  
### [Mieux comprendre les cycles de vie](DependencyInjection.md#mieux-comprendre-les-cycles-de-vie)  