# [MasterClass WebApi .NET Core - Introduction](README.md)  

## Injection de dépendances  

### Introduction  

L’injection de dépendances est une technique de couplage faible entre des classes et leurs dépendances.  
La création d'une instance sera déportée de l'objet d'où elle sera utilisée. Il existe différents types de cycle de vie de ces instances :

- Transient : Instance créée à la demande
- Scoped : Instance créée à la requête
- Singleton : Instance créée à la première demande puis réutilisée sur toutes les autres  

### Injection de dépendance dans un middleware  

Afin de comprendre et illustrer ce principe important nous allons créer un nouveau middleware qui s'occupera de renseigner des informations de contexte applicatif dans le header de la réponse (en l'occurence un id lié à la requête pour l'exemple)

Création d'une interface pour le contexte applicatif
```c#
public interface IApplicationContext
{
    Guid Id { get; }
}
```

Création de la classe qui implémente l'interface
```c#
public class McContext : IApplicationContext
{
    public McContext()
    {
        this.Id = Guid.NewGuid();

    }
    public Guid Id { get; }
}
```

Création du middleware
```c#
public class TrackApplicationContextMiddleware
{
    private readonly RequestDelegate _next;

    public TrackApplicationContextMiddleware(RequestDelegate next)
    {
        _next = next;
    }
    
    public async Task InvokeAsync(HttpContext context, IApplicationContext appContext)
    {
        context.Response.Headers.Add("X-Guid", appContext.Id.ToString());
        await _next(context);
    }
}
```

Dans la classe *StartUp* nous ajoutons ce middleware
```c#
app.UseMiddleware<TrackApplicationContextMiddleware>();
```

Et nous choisissons de résoudre notre interface sur un cycle de vie *Scope* de manière à ce que notre Guid soit crée à chaque requête
```c#
services.AddScoped<IApplicationContext, McContext>();
```

Nous voyons alors apparaitre un X-Guid dans le header de notre réponse qui change à chaque nouvelle requête.

### Mieux comprendre les cycles de vie  

Amusons nous avec l'injection de dépendance pour mieux comprendre les cycles de vie de nos objets.  
Afin de vérifier que le périmètre scope est bien respecté, ajoutons notre *IApplicationContext* dans le constructeur de notre controlleur et comparons les guid
```c#
[Route("api/_system")]
public class DiagnosticController : Controller
{
    private readonly DiagnosticOptions _options;
    private readonly IApplicationContext _appContext;

    public DiagnosticController(IOptionsMonitor<DiagnosticOptions> options, IApplicationContext appContext)
    {
        _options = options.CurrentValue;
        _appContext = appContext;
    }

    [HttpGet, HttpHead, Route("healthcheck")]
    public IActionResult HealthCheck()
    {
        ControllerContext.HttpContext.Response.Headers.Add("X-Guid2", _appContext.Id.ToString());
        return Ok(_options.HealthCheckContent);
    }
}
```

Pour aller plus loin changez l'enregistrement dans notre conteneur d'injection de dépendances sur un mode *Transient*
```c#
services.AddTransient<IApplicationContext, McContext>();
```
On constate alors que le Guid est différent entre notre controlleur et notre middleware

Changez de nouveau l'enregistrement pour revenir à un périmètre *Scope*.  
Maintenant nous allons changer notre middleware de manière à ce que la résolution se fasse au niveau du constructeur au lieu de la méthode
```c#
public class TrackApplicationContextMiddleware
{
    private readonly RequestDelegate _next;
    private readonly IApplicationContext _appContext;

    public TrackApplicationContextMiddleware(RequestDelegate next, IApplicationContext appContext)
    {
        _next = next;
        _appContext = appContext;
    }
    
    public async Task InvokeAsync(HttpContext context)
    {
        context.Response.Headers.Add("X-Guid", _appContext.Id.ToString());
        await _next(context);
    }
}
```
La résolution de dépendance plante au démarrage de l'appli. Car l'instance du middleware est un singleton.  
```An unhandled exception of type 'System.InvalidOperationException' occurred in Microsoft.AspNetCore.Hosting.dll: 'Cannot resolve scoped service 'MasterClass.Core.Context.IApplicationContext' from root provider.'```  

Il faut donc changer le mode d'enregistrement de notre instance.  
```c#
services.AddSingleton<IApplicationContext, McContext>();
```

Nous constatons que le Guid est alors identique sur l'ensemble de nos requêtes...

[Retour au menu](README.md)