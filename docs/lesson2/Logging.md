# [MasterClass WebApi .NET Core - Structure et documentation](README.md)  

## Logging  

Nous allons maintenant voir comment fonctionne le système de logging dans .Net Core.  
Lorsque nous utilisons le *CreateDefaultBuilder* dans notre classe *Program* la configuration de logging par défaut qui est appliqué est la suivante
```c#
.ConfigureLogging((Action<WebHostBuilderContext, ILoggingBuilder>) ((hostingContext, logging) =>
{
    logging.AddConfiguration((IConfiguration) hostingContext.Configuration.GetSection("Logging"));
    logging.AddConsole();
    logging.AddDebug();
}));
```

Il est possible de rajouter un fournisseur de log en appelant la méthode d'extension *ConfigureLogging* sur notre *WebHostBuilder* et d'ajouter un singleton sur une classe qui implémente *ILoggerProvider*

Voici par exemple ce que fait la méthode d'extensions *AddConsole*
```c#
public static ILoggingBuilder AddConsole(this ILoggingBuilder builder)
{
    builder.Services.AddSingleton<ILoggerProvider, ConsoleLoggerProvider>();
    return builder;
}
```

C'est bien de le savoir mais dans les faits le plus pratique est d'écrire dans la sortie standard  
Nous pouvons ensuite récupérer la sortie standard et par exemple la pousser dans un elasticsearch puis dans un kibana (je ne vais pas rentrer dans les détails, voici [un lien intéressant](https://logz.io/blog/docker-logging/) si vous voulez creuser le sujet)

Quoiqu'il en soit nous allons voir comment utiliser la fonctionnalité de logging. Cela fonctionne naturellement via l'injection de dépendance  
Si nous reprenons le middleware précédent, nous décidons d'écrire ces informations dans un log plutot que dans le header de la réponse
```c#
public class TrackApplicationContextMiddleware
{
    private readonly RequestDelegate _next;
    private readonly ILogger<TrackApplicationContextMiddleware> _logger;

    public TrackApplicationContextMiddleware(RequestDelegate next, ILogger<TrackApplicationContextMiddleware> logger)
    {
        _next = next;
        _logger = logger;
    }
    
    public async Task InvokeAsync(HttpContext context, IApplicationContext appContext)
    {
        _logger.LogInformation($"X-Guid : {appContext.Id}");
        await _next(context);
    }
}
```

Si vous ne voyez rien dans votre console, vérifier la config des logs (cela permet de définir le niveau minmum par défaut que l'on veut appliquer pour écrire les logs)
```json
"Console": {
    "LogLevel": {
        "Default": "Information"
    }
}
```

[Page suivante : Structure du projet](Project.md)  
[Retour au menu](README.md)