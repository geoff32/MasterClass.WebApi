# [MasterClass WebApi .NET Core - Structure et documentation](README.md)  

## Swagger  

### Introduction    

Swagger va nous permettre d'exposer une documentation de notre API.
Dans le cadre de notre projet nous ne souhaitons pas que cette documentation soit visible dans l'environnement de production (par exemple l'API n'est utilisée que sur des applicatifs développés en interne, pas besoin de documenter à l'extérieur du coup)

### Documentation  

Nous allons ajouter le package *Swashbuckle.AspNetCore* à notre WebApi
> dotnet add ./src/MasterClass.WebApi/MasterClass.WebApi.csproj package Swashbuckle.AspNetCore

Mise en place d'une configuration pour activer/désactiver swagger
```c#
public class SwaggerOptions
{
    public bool Enabled { get; set; }
}
```

Dans *appsettings.json*
```json
"SwaggerOptions": {
  "Enabled": true
}
```

On le désactive pour la production dans *appsettings.Production.json*
```json
"SwaggerOptions": {
  "Enabled": false
}
```

On crée une méthode d'extension qui va permettre de générer un document décrivant notre API et d'enregistrer notre configuration
```c#
private const string VERSION = "v1";
private const string SWAGGER_DOCNAME = "masterclass.webapi." + VERSION;

public static IServiceCollection AddMasterClassSwagger(this IServiceCollection services, IConfiguration config)
{
    var swaggerSection = config.GetSection(nameof(SwaggerOptions));
    services.Configure<SwaggerOptions>(swaggerSection);

    var options = swaggerSection.Get<SwaggerOptions>();
    if (options.Enabled)
    {
        services.AddSwaggerGen(genOptions
            => genOptions.SwaggerDoc(SWAGGER_DOCNAME,
                new Swagger.Info
                {
                    Title = "MasterClass WebApi",
                    Version = VERSION
                }));
    }

    return services;
}
```

On crée une autre méthode d'extension qui va permettre d'utiliser l'UI de swagger.  
*UseSwagger* permet d'exposer le doc généré ci dessus sous forme d'un json et *UseSwaggerUI* permet d'utiliser l'UI swagger
```c#
public static IApplicationBuilder UseMasterClassSwaggerUI(this IApplicationBuilder app, SwaggerOptions options)
{
    if (options.Enabled)
    {
        app.UseSwagger();

        app.UseSwaggerUI(uiOptions => uiOptions.SwaggerEndpoint($"/swagger/{SWAGGER_DOCNAME}/swagger.json", "MasterClass WebApi"));
    }

    return app;
}
```

Dans la méthode *ConfigureServices* de la classe StartUp
```c#
services.AddMasterClassSwagger(Configuration);
```

Dans la méthode *Configure* nous passons les options par injection de dépendance et ajoutons les middleware
```c#
public void Configure(IApplicationBuilder app, IHostingEnvironment env, IOptions<SwaggerOptions> swaggerOptions)
{
    ...
    app.UseMasterClassSwaggerUI(swaggerOptions.Value);
}
```

[Retour au menu](README.md)