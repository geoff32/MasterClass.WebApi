# [MasterClass WebApi .NET Core - Structure et documentation](README.md)  

## Authentification  

Notre objectif est de créer une route qui va permettre de valider les informations d'authentification de l'utilisateur
> POST api/user/authenticate

Nous allons pour cela implémenter les différentes couches de notre applicatif

### Repository  

Nous n'allons pas nous interfacer avec une base de données mais utiliser un mock d'une base d'utilisateur

Ajout d'un fichier de settings pour les mock
> mkdir ./src/MasterClass.WebApi/Mock  
echo. > ./src/MasterClass.WebApi/Mock/users.json

```json
{
    "MockUsers": {
        "Users": [{
            "Id": 1,
            "Login": "login1",
            "Password": "pwd1",
            "Name": "name1",
            "Roles": [
                "SuperAdmin",
                "Admin",
                "User"
            ]
        },
        {
            "Id": 2,
            "Login": "login2",
            "Password": "pwd2",
            "Name": "name2",
            "Roles": [
                "Admin",
                "User"
            ]
        },
        {
            "Id": 3,
            "Login": "login3",
            "Password": "pwd3",
            "Name": "name3",
            "Roles": [
                "User"
            ]
        }]
    }
} 
```

Création du modèle User
> mkdir ./src/MasterClass.Repository/Users  
mkdir ./src/MasterClass.Repository/Users/Models  
echo. > ./src/MasterClass.Repository/Users/Models/User.cs  

```c#
namespace MasterClass.Repository.Users.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string[] Roles { get; set; }
    }
}   
```

Création de l'object Mock

> mkdir ./src/MasterClass.Repository/Users/Mock  
echo. > ./src/MasterClass.Repository/Users/Mock/MockUsers.cs  

```c#
using System.Collections.Generic;
using MasterClass.Repository.Users.Models;

namespace MasterClass.Repository.Users.Mock
{
    public class MockUsers
    {
        public List<User> Users { get; set; }
    }
} 
```

Nous allons installer le package Microsoft.Extensions.Options dans le projet MasterClass.Repository afin d'utiliser la résolution de notre fichier de config

> dotnet add ./src/MasterClass.Repository/MasterClass.Repository.csproj package Microsoft.Extensions.Options  

Nous créons une interface pour notre repository, qui expose une méthode permettant de récupérer un utilisateur via son login

> mkdir ./src/MasterClass.Repository/Users/Contracts  
echo. > ./src/MasterClass.Repository/Users/Contracts/IUserRepository.cs  

```c#
using MasterClass.Repository.Users.Models;

namespace MasterClass.Repository.Users
{
    public interface IUserRepository
    {
         User GetUser(string login);
    }
}
```

Et une implémentation de cette interface qui se base sur notre mock
> echo. > ./src/MasterClass.Repository/Users/Mock/MockUserRepository.cs  

```c#
using MasterClass.Repository.Users.Contracts;
using MasterClass.Repository.Users.Models;
using Microsoft.Extensions.Options;
using System.Linq;

namespace MasterClass.Repository.Users.Mock
{
    public class MockUserRepository : IUserRepository
    {
        private readonly MockUsers _mock;

        public MockUserRepository(IOptions<MockUsers> mock) => _mock = mock.Value;

        public User GetUser(string login) => _mock.Users.SingleOrDefault(user => user.Login == login);
    }
}
```

### Business  

Notre couche business va s'occuper d'exposer une méthode permettant de savoir si les informations d'authentification sont valides

> mkdir ./src/MasterClass.Business/Users  
mkdir ./src/MasterClass.Business/Users/Contracts  
echo. > ./src/MasterClass.Business/Users/Contracts/IUserBusiness.cs  

```c#
using MasterClass.Repository.Users.Models;

namespace MasterClass.Business.Users.Contracts
 {
     public interface IUserBusiness
    {
        User AuthenticateUser(string login, string password);
    }
 }
```

L'implémentation de notre interface va s'appuyer sur notre couche de repository
> echo. > ./src/MasterClass.Business/Users/UserBusiness.cs  

```c#
using MasterClass.Business.Users.Contracts;
using MasterClass.Repository.Users.Contracts;
using MasterClass.Repository.Users.Models;

namespace MasterClass.Business.Users
{
    public class UserBusiness : IUserBusiness
    {
        private readonly IUserRepository _userRepository;

        public UserBusiness(IUserRepository userRepository) => _userRepository = userRepository;

        public User AuthenticateUser(string login, string password)
        {
            var user = _userRepository.GetUser(login);
            return user != null && user.Password == password ? user : null;
        }
    }
}
```

### Service  

Notre couche service va s'occuper de formatter le résultat de la couche business  

Création de notre contrat de sortie
> mkdir ./src/MasterClass.Service/Users  
mkdir ./src/MasterClass.Service/Users/Contracts  
mkdir ./src/MasterClass.Service/Users/Contracts/Models    
echo. > ./src/MasterClass.Service/Users/Contracts/Models/AuthenticatedUser.cs
```c#
namespace MasterClass.Service.Users.Contracts.Models
{
    public interface IAuthenticatedUser
    {
        int Id { get; }
    }
}
```

Création de notre modèle en sortie avec une static factory
> mkdir ./src/MasterClass.Service/Users/Models  
echo. > ./src/MasterClass.Service/Users/Models/AuthenticatedUser.cs
```c#
using MasterClass.Repository.Users.Models;
using MasterClass.Service.Users.Contracts.Models;

namespace MasterClass.Service.Users.Models
{
    public class AuthenticatedUser : IAuthenticatedUser
    {
        public int Id { get; }

        private AuthenticatedUser(int id)
        {
            Id = id;
        }

        internal static AuthenticatedUser Create(User user)
            => user == null ? null : new AuthenticatedUser(user.Id);
    }
}
```

Création de notre modèle en entrée
> echo. > ./src/MasterClass.Service/Users/Models/AuthenticateParameters.cs
```c#
 namespace MasterClass.Service.Users.Models
 {
     public class AuthenticateParameters
     {
         public string Login { get; set; }
         public string Password { get; set; }
     }
 }
```

Notre interface de service
> echo. > ./src/MasterClass.Service/Users/Contracts/IUserService.cs  
```c#
using MasterClass.Service.Users.Contracts.Models;
using MasterClass.Service.Users.Models;

namespace MasterClass.Service.Users.Contracts
{
    public interface IUserService
    {
        IAuthenticatedUser Authenticate(AuthenticateParameters authParams);
    }
}
```

L'implémentation de notre interface de service
> echo. > ./src/MasterClass.Service/Users/UserService.cs  
```c#
using MasterClass.Business.Users.Contracts;
using MasterClass.Service.Users.Contracts;
using MasterClass.Service.Users.Contracts.Models;
using MasterClass.Service.Users.Models;

namespace MasterClass.Service.Users
{
    public class UserService : IUserService
    {
        private readonly IUserBusiness _userBusiness;

        public UserService(IUserBusiness userBusiness) => _userBusiness = userBusiness;

        public IAuthenticatedUser Authenticate(AuthenticateParameters authParams)
        {
            return AuthenticatedUser.Create(_userBusiness.AuthenticateUser(authParams.Login, authParams.Password));
        }
    }
}
```

### WebApi  

Création de notre controller qui va se base sur notre couche de service
> echo. > ./src/MasterClass.WebApi/Controllers/UserController.cs  

```c#
using MasterClass.Service.Users.Contracts;
using MasterClass.Service.Users.Models;
using Microsoft.AspNetCore.Mvc;

namespace MasterClass.WebApi.Controllers
{
    [Route("api/user")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService) => _userService = userService;

        [HttpPost, Route("authenticate")]
        public IActionResult Authenticate([FromBody]AuthenticateParameters authParams)
        {
            var authUser = _userService.Authenticate(authParams);
            return authUser == null ? (IActionResult)Unauthorized() : Ok(authUser);
        }
    }
}
```


### Résolution par injection de dépendances  

Il ne nous reste plus qu'à enregistrer nos différentes couches dans notre conteneur d'injection de dépendances. Par souci d'organisation nous allons créer une méthode d'extension pour chacune de nos couches.


Création de méthodes d'extension pour enregister notre mock
> mkdir ./src/MasterClass.WebApi/StartupExtensions  
echo. > ./src/MasterClass.WebApi/StartupExtensions/MockExtensions.cs  
```c#
using MasterClass.Repository.Users.Mock;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MasterClass.WebApi.StartupExtensions
{
    public static class MockExtensions
    {
        public static IWebHostBuilder UseMockFiles(this IWebHostBuilder hostBuilder)
        {
            hostBuilder.ConfigureAppConfiguration((builderContext, configBuilder) => 
            {
                configBuilder.AddJsonFile("Mock/users.json");
            });

            return hostBuilder;
        }

        public static IServiceCollection ConfigureMock(this IServiceCollection services, IConfiguration config)
        {
            services.Configure<MockUsers>(config.GetSection(nameof(MockUsers)));

            return services;
        }
    }
}
```

Création d'une méthode d'extension pour enregister notre couche repository
> echo. > ./src/MasterClass.WebApi/StartupExtensions/RepositoryExtensions.cs  

```c#
using MasterClass.Repository.Users.Contracts;
using MasterClass.Repository.Users.Mock;
using Microsoft.Extensions.DependencyInjection;

namespace MasterClass.WebApi.StartupExtensions
{
    public static class RepositoryExtensions
    {
        public static IServiceCollection AddRepository(this IServiceCollection services)
        {
            services.AddSingleton<IUserRepository, MockUserRepository>();

            return services;
        }
    } 
}
```

Création d'une méthode d'extension pour enregister notre couche business
> echo. > ./src/MasterClass.WebApi/StartupExtensions/BusinessExtensions.cs  

```c#
using MasterClass.Business.Users.Contracts;
using MasterClass.Business.Users;
using Microsoft.Extensions.DependencyInjection;

namespace MasterClass.WebApi.StartupExtensions
{
    public static class BusinessExtensions
    {
        public static IServiceCollection AddBusiness(this IServiceCollection services)
        {
            services.AddSingleton<IUserBusiness, UserBusiness>();

            return services;
        }
    } 
}
```

Création d'une méthode d'extension pour enregister notre couche service
> echo. > ./src/MasterClass.WebApi/StartupExtensions/ServiceExtensions.cs  

```c#
using MasterClass.Service.Users.Contracts;
using MasterClass.Service.Users;
using Microsoft.Extensions.DependencyInjection;

namespace MasterClass.WebApi.StartupExtensions
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddService(this IServiceCollection services)
        {
            services.AddSingleton<IUserService, UserService>();

            return services;
        }
    } 
}
```

Dans la classe *Program*  

```c#
public static IWebHost BuildWebHost(string[] args) =>
    WebHost.CreateDefaultBuilder(args)
        .UseStartup<Startup>()
        .UseMockFiles()
        .Build();
```

Dans la classe *Startup*
```c#
services.ConfigureMock(Configuration);

services.AddRepository();
services.AddBusiness();
services.AddService();
```

[Page suivante : Json Web Token](Jwt.md)  
[Retour au menu](README.md)
