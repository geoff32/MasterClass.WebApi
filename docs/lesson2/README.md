# MasterClass WebApi .NET Core - Structure et documentation

## [Logging](Logging.md#logging) (10min)

## [Structure du projet](Project.md#structure-du-projet) (15min)
### [Repository](Project.md#repository)
### [Business](Project.md#business)
### [Service](Project.md#service)
### [Interdépendance](Project.md#interdépendance)

## [Authentification](Authentification.md#authentification) (45min)
### [Repository](Authentification.md#repository)
### [Business](Authentification.md#business)
### [Service](Authentification.md#service)
### [WebApi](Authentification.md#webapi)
### [Résolution par injection de dépendances](Authentification.md#résolution-par-injection-de-dépendances)

## [Swagger](Swagger.md#swagger) (15min)
### [Introduction](Swagger.md#introduction)
### [Documentation](Swagger.md#documentation)