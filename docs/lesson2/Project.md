# [MasterClass WebApi .NET Core - Structure et documentation](README.md)  

## Structure du projet  

### Repository  

Ce projet va contenir :

- les modèles de données du repository
- les interfaces de repository
- les implémentations des interfaces de repository
Création du projet :

> cd src  
dotnet new classlib -n MasterClass.Repository  
cd..  
dotnet sln add src/MasterClass.Repository/MasterClass.Repository.csproj

### Business  

Ce projet va appeler la couche de repository et appliquer des règles métiers sur les datas

> cd src  
dotnet new classlib -n MasterClass.Business  
cd..  
dotnet sln add src/MasterClass.Business/MasterClass.Business.csproj

### Service  

Ce projet va contenir les modèles de données de notre API ainsi que les contrats d'échanges.
Il contiendra également les mappers qui permettront de transformer les objets récupérés via la couche Business en objets exposés par notre API

> cd src  
dotnet new classlib -n MasterClass.Service  
cd..  
dotnet sln add src/MasterClass.Service/MasterClass.Service.csproj

### Dépendances entre les projets  

> dotnet add src/MasterClass.Business/MasterClass.Business.csproj reference src/MasterClass.Repository/MasterClass.Repository.csproj  
dotnet add src/MasterClass.Service/MasterClass.Service.csproj reference src/MasterClass.Business/MasterClass.Business.csproj  
dotnet add src/MasterClass.WebApi/MasterClass.WebApi.csproj reference src/MasterClass.Service/MasterClass.Service.csproj

La dépendance entre nos couches va s'organiser via injection de constructeurs :

- Le constructeur de notre controller va prendre en paramètre une instance de notre objet service
- Le constructeur de notre service va prendre en paramètre une instance de notre objet business
- Le constructeur de notre business va prendre en paramètre une instance de notre Repository  

L'injection de dépendance permettra de créer ces différentes instances

[Page suivante : Authentification](Authentification.md)  
[Retour au menu](README.md)
