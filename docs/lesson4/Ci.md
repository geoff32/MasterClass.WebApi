# [MasterClass WebApi .NET Core - Docker, Tests et CI](README.md)  

## CI  

Nous allons ajouter une CI sur gitlab. Si vous voulez voir le résultat de la CI il vous faudra un compte sur gitlab et connecter votre repository dessus si il n'y est pas déjà  

### Compilation et exécution des tests

Ajouter le fichier
> echo. >.gitlab-ci.yml  
```yml
image: microsoft/dotnet:latest

stages:
  - build
  - test

variables:
  project: "MasterClass.WebApi"

before_script:
  - "dotnet restore"

build:
  stage: build
  variables:
    build_path: "src/$project"
  script:
    - "cd $build_path"
    - "dotnet build"

test:
  stage: test
  variables:
    test_path: "test"
  script:
    - "cd $test_path"
    - "dotnet test"
```

Notre pipeline comprends 2 jobs :
- build : Build notre projet
- test : Lance les tests  

### Publication d'images docker

Nous allons maintenant utiliser notre ci pour pousser des images dans dockerhub  

Nous allons éditer notre fichier *Dockerfile* qui nous permet de publier le site en release de manière à inclure les tests  
```dockerfile
FROM microsoft/aspnetcore-build:2.0 AS build-env
WORKDIR /app

# copy everything and restore
COPY ./ ./
RUN dotnet restore

# run tests
WORKDIR /app/test
RUN dotnet test

# publish
WORKDIR /app/src
RUN dotnet restore MasterClass.WebApi/MasterClass.WebApi.csproj 
RUN dotnet publish MasterClass.WebApi/MasterClass.WebApi.csproj -c Release -o out --force

# build runtime image
FROM microsoft/aspnetcore:2.0
WORKDIR /app/Masterclass.WebApi
COPY --from=build-env /app/src/MasterClass.WebApi/out .
ENTRYPOINT dotnet MasterClass.WebApi.dll
```

En utilisant le service Docker in Docker, nous allons pouvoir utiliser docker dans notre ci gitlab.  
Modifions notre fichier *.gitlab-ci.yml*
```yml
image: docker:latest

variables:
  DOCKER_DRIVER: overlay
  DOCKER_IMAGE: geoff32/masterclass.webapi
  CONTAINER_TEST_IMAGE: $DOCKER_IMAGE:$CI_BUILD_REF_NAME

services:
- docker:dind

stages:
- build
- publish

build:
  stage: build
  script:
    - docker build -t $CONTAINER_TEST_IMAGE -f Dockerfile .
    - docker login -u $CI_DOCKERHUB_USR -p $CI_DOCKERHUB_PWD
    - docker push $CONTAINER_TEST_IMAGE
  
publish:
  stage: publish
  script: 
    - docker login -u $CI_DOCKERHUB_USR -p $CI_DOCKERHUB_PWD
    - docker pull $CONTAINER_TEST_IMAGE
    - docker image tag $CONTAINER_TEST_IMAGE $DOCKER_IMAGE
    - docker push $DOCKER_IMAGE
  only:
    - master
```

Le stage build permet de jouer les tests puis publier la branche en cours dans notre dockerhub avec un tag correspondant à la branche.  
Le stage publish permet de récupérer le tag issu de la branche pour le pousser sur le latest. Ce dernier ne s'exécutera que sur la branche master  
Les variables CI_DOCKERHUB_USR et CI_DOCKERHUB_PWD sont définies en variables dans notre CI de manière à ne pas les rendre publique  

Il ne nous reste plus qu'à modifier notre *docker-compose.tml* de manière à monter les conteneurs à partir de l'image latest sur notre dockerhub
```yml
version: '2.1'

services:
  masterclass:
    image: geoff32/masterclass.webapi:latest
    ports:
      - 80:80
    environment:
      - ASPNETCORE_ENVIRONMENT=Production
```

Nous pourrions même pousser plus loin en intégrant du Continous Delivery dans notre CI de manière à publier chaque branche automatiquement sur une url dédiée à chaque commit  

[Retour au menu](README.md)