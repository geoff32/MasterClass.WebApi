# [MasterClass WebApi .NET Core - Docker, Tests et CI](README.md)  

## Docker  

La portabilité de .NET Core nous permet de faire tourner notre application sur un docker. Nous allons automatiser la création de conteneurs à la fois en debug mais aussi en production

### Environnement de debug  

Création d'un fichier Dockerfile.debug
> echo. > Dockerfile.debug

```Dockerfile
FROM microsoft/aspnetcore-build:2.0 AS build-env
RUN mkdir /app
WORKDIR /app

COPY ./src/ ./
RUN dotnet restore MasterClass.WebApi/MasterClass.WebApi.csproj
RUN dotnet publish -c Debug MasterClass.WebApi/MasterClass.WebApi.csproj -o out --force

#Install debugger
FROM microsoft/aspnetcore:2.0
ENV NUGET_XMLDOC_MODE skip
WORKDIR /vsdbg
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
       unzip \
    && rm -rf /var/lib/apt/lists/* \
    && curl -sSL https://aka.ms/getvsdbgsh | bash /dev/stdin -v latest -l /vsdbg
WORKDIR /app/MasterClass.WebApi/out
COPY --from=build-env /app/MasterClass.WebApi/out .
# Kick off a container just to wait debugger to attach and run the app
ENTRYPOINT ["/bin/bash", "-c", "sleep infinity"]
```

Afin de profiter du formatage et de l'intellisense de l'extension docker vous pouvez rajouter un fichier de settings
> echo. > ./.vscode/settings.json

```json
{
    "files.associations": {
        "dockerfile.*": "dockerfile"
    }
}
```

Nous utilisons un fichier docker-compose afin de construire l'image et le container docker utilisé dans le debug
> echo. > docker-compose.debug.yml

```yml
version: '2.1'

services:
  masterclass_debug:
    image: masterclass.webapi:debug
    container_name: masterclass-webapi
    build:
      context: .
      dockerfile: Dockerfile.debug
    ports:
      - 5000:80
    environment:
      - ASPNETCORE_ENVIRONMENT=Development
      - DOTNET_USE_POLLING_FILE_WATCHER=1
    entrypoint: tail -f /dev/null 
```

Ajout d'une tache dans le fichier *tasks.json* qui lancera le docker-compose

```json
{
    "label": "buildDocker",
    "command": "docker-compose",
    "type": "process",
    "args": [
        "-f",
        "${workspaceFolder}/docker-compose.debug.yml",
        "up",
        "-d",
        "--build"
    ],
    "problemMatcher": "$msCompile"
}
```

Ajout d'un configuration dans le fichier *launch.json*

```json
{
    "name": ".NET Core Docker Launch (console)",
    "type": "coreclr",
    "request": "launch",
    "preLaunchTask": "buildDocker",
    "program": "/app/MasterClass.WebApi/out/MasterClass.WebApi.dll",
    "cwd": "/app/MasterClass.WebApi/out",
    "sourceFileMap": {
        "/app": "${workspaceRoot}"
    },
    "pipeTransport": {
        "pipeProgram": "docker",
        "pipeCwd": "${workspaceRoot}",
        "pipeArgs": [
            "exec -i masterclass-webapi"
        ],
        "quoteArgs": false,
        "debuggerPath": "/vsdbg/vsdbg"
    }
}
```

Vous pouvez à présent débugguer la solution à la fois dans votre environnement mais aussi dans un docker

### Environnement de production  

Comme vous avez pu le constater nous avons construit dans l'étape précédente un conteneur docker sur lesquel est installé en plus du framework aspnet core le debugger visual studio. De plus la solution est publié en mode debug.  
Nous allons créer un configuration de déploiement pour l'environnement de production
> echo. > Dockerfile

```Dockerfile
FROM microsoft/aspnetcore-build:2.0 AS build-env
WORKDIR /app

# copy everything and build
COPY ./src/ ./
RUN dotnet restore MasterClass.WebApi/MasterClass.WebApi.csproj 
RUN dotnet publish MasterClass.WebApi/MasterClass.WebApi.csproj -c Release -o out --force

# build runtime image
FROM microsoft/aspnetcore:2.0
WORKDIR /app/Masterclass.WebApi
COPY --from=build-env /app/MasterClass.WebApi/out .
ENTRYPOINT dotnet MasterClass.WebApi.dll
```

> echo. > docker-compose.yml

```yml
version: '2.1'

services:
  masterclass:
    image: masterclass.webapi
    build:
      context: .
      dockerfile: Dockerfile
    ports:
      - 80:80
    environment:
      - ASPNETCORE_ENVIRONMENT=Production
```

Le conteneur peut être monté en utilisant docker-compose
> docker-compose -f docker-compose.yml up -d --build

Et détruit de la manière suivante
> docker-compose -f docker-compose.yml down

[Page suivante : Tests](Test.md)  
[Retour au menu](README.md)