# [MasterClass WebApi .NET Core - Docker, Tests et CI](README.md)  

## Tests  

### Initialisation de la solution de test  

Création d'une solution de tests
> mkdir test  
cd test  
dotnet new sln -n MasterClass.WebApi.Test  

Ajout d'un projet de test dans le répertoire test  
> dotnet new xunit -n MasterClass.WebApi.Test  
dotnet sln add ./MasterClass.WebApi.Test/MasterClass.WebApi.Test.csproj  

Nous référençons le projet à tester dans le projet de test
> cd ..  
dotnet add ./test/MasterClass.WebApi.Test/MasterClass.WebApi.Test.csproj reference ./src/MasterClass.WebApi/MasterClass.WebApi.csproj

Afin de bénéficier de l'intellisense de l'extension omnisharp nous le rajoutons dans notre fichier *MasterClass.WebApi.sln*
> dotnet sln add ./test/MasterClass.WebApi.Test/MasterClass.WebApi.Test.csproj  

Ajout du package *Moq* qui nous permettra de mocker nos objets
> dotnet add ./test/MasterClass.WebApi.Test/MasterClass.WebApi.Test.csproj package Moq   

Si vous avez installé l'extension *.NET Core Test Explorer* il faut rajouter cette config dans le fichier *settings.json* (répertoire *.vscode*)  
```json
"dotnet-test-explorer.testProjectPath": "test"
```
### Test unitaire  

Ajout d'un builder pour générer notre *Fixture* (fixe un environnement pour l'exécution des tests)
> mkdir ./test/MasterClass.WebApi.Test/Controllers  
mkdir ./test/MasterClass.WebApi.Test/Controllers/Fixtures  
echo. > ./test/MasterClass.WebApi.Test/Controllers/Fixtures/UserControllerFixtureBuilder.cs
```c#
using System;
using MasterClass.Service.Users.Contracts;
using MasterClass.Service.Users.Contracts.Models;
using MasterClass.Service.Users.Models;
using MasterClass.WebApi.Controllers;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;
using Moq;

namespace MasterClass.WebApi.Test.Controllers.Fixtures
{
    public class UserControllerFixtureBuilder
    {
        private ServiceCollection _services;
        private readonly Mock<IUserService> _mockUserService;

        public UserControllerFixtureBuilder()
        {
            _services = new ServiceCollection();
            _mockUserService = new Mock<IUserService>();
        }

        public UserControllerFixtureBuilder Initialize()
        {
            _services.Clear();
            
            _services.AddSingleton<IUserService>(_mockUserService.Object);
            _services.AddSingleton<ISystemClock, SystemClock>();
            _services.AddTransient<UserController>(sp => new UserController(sp.GetService<IUserService>(), sp.GetService<ISystemClock>()));

            return this;
        }

        public UserControllerFixtureBuilder AddValidAuthentication(AuthenticateParameters authParams, IAuthenticatedUser user)
        {
            _mockUserService.Setup(userService => userService.Authenticate(authParams, It.IsAny<DateTimeOffset>()))
                .Returns(user);

            return this;
        }

        public UserControllerFixtureBuilder AddInvalidAuthentication(AuthenticateParameters authParams)
        {
            _mockUserService.Setup(userService => userService.Authenticate(authParams, It.IsAny<DateTimeOffset>()))
                .Returns((IAuthenticatedUser)null);
                
            return this;
        }

        public IServiceProvider Build()
        {
            return _services.BuildServiceProvider();
        }
    }
}
```

Ajout d'une classe de test pour tester notre controller *UserController*
> echo. > ./test/MasterClass.WebApi.Test/Controllers/UserController.test.cs  
```c#
using MasterClass.Service.Users.Contracts.Models;
using MasterClass.Service.Users.Models;
using MasterClass.WebApi.Controllers;
using MasterClass.WebApi.Test.Controllers.Fixtures;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Xunit;

namespace MasterClass.WebApi.Test.Controllers
{
    public class UserControllerTest : IClassFixture<UserControllerFixtureBuilder>
    {
        private readonly UserControllerFixtureBuilder _fixtureBuilder;

        public UserControllerTest(UserControllerFixtureBuilder fixtureBuilder)
        {
            _fixtureBuilder = fixtureBuilder;
        }

        #region Authenticate
        [Fact]
        public void Authenticate_Valid()
        {
            //Given
            var authParams = Mock.Of<AuthenticateParameters>();
            var authUser = Mock.Of<IAuthenticatedUser>();

            var userController = _fixtureBuilder
                .Initialize()
                .AddValidAuthentication(authParams, authUser)
                .Build().GetService<UserController>();

            //When
            var actionResult = userController.Authenticate(authParams);

            //Then
            Assert.IsAssignableFrom<OkObjectResult>(actionResult);
            var model = (actionResult as OkObjectResult)?.Value;
            Assert.IsAssignableFrom<IAuthenticatedUser>(model);
            Assert.NotNull(model);
            Assert.Equal(authUser, model);
        }

        [Fact]
        public void Authenticate_Invalid()
        {
            //Given
            var authParams = Mock.Of<AuthenticateParameters>();
            
            var userController = _fixtureBuilder
                .Initialize()
                .AddInvalidAuthentication(authParams)
                .Build().GetService<UserController>();

            //When
            var actionResult = userController.Authenticate(authParams);

            //Then
            Assert.IsAssignableFrom<UnauthorizedResult>(actionResult);
            Assert.NotNull(actionResult);
        }

        #endregion
    }
}
```

Si vous n'avez pas installé l'extension *.NET Core Test Explorer* vous pouvez jouer les tests en utilisant l'intellisense de l'extension Omnisharp (actions possible sur les controlleurs et méthodes de test) ou en ligne de commande  
> dotnet test test  

### Test d'intégration  

Ajout du package *Microsoft.AspNetCore.Mvc.Testing* qui nous permettra de tester notre API
> dotnet add ./test/MasterClass.WebApi.Test/MasterClass.WebApi.Test.csproj package Microsoft.AspNetCore.Mvc.Testing  

Ajout d'une classe de test afin de vérifier que notre test de vie répond sur la bonne route
> echo. > ./test/MasterClass.WebApi.Test/Controllers/DiagnosticController.test.cs  

```c#
using System.Net;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace MasterClass.WebApi.Test.Controllers
{
    public class DiagnosticControllerTest : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public DiagnosticControllerTest(WebApplicationFactory<Startup> fixture)
        {
            _client = fixture.CreateClient();
        }

        [Fact]
        public async void HealthCheck_Get_Status200()
        {
            var response = await _client.GetAsync("api/_system/healthcheck");

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal("system_ok", await response.Content.ReadAsStringAsync());
        }

        [Fact]
        public async void HealthCheck_Head_Status200()
        {
            var response = await _client.SendAsync(new HttpRequestMessage(HttpMethod.Head, "api/_system/healthcheck"));

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
    }
}
```

[Page suivante : CI](Ci.md)  
[Retour au menu](README.md)