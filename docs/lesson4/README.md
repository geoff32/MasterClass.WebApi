# MasterClass WebApi .NET Core - Docker, Tests et CI  

## [Docker](Docker.md#docker) (25min)  
### [Environnement de debug](Docker.md#environnement-de-debug)  
### [Environnement de production](Docker.md#environnement-de-production)  

## [Tests](Test.md#tests) (30min)  
### [Initialisation de la solution de test](Test.md#initialisation-de-la-solution-de-test)  
### [Test unitaire](Test.md#test-unitaire)  
### [Test d'intégration](Test.md#test-dintégration)  

## [CI](Ci.md#ci)  (30min)
### [Compilation et exécution des tests](Ci.md#compilation-et-exécution-des-tests)  
### [Publication d'images docker](Ci.md#publication-dimages-docker)  