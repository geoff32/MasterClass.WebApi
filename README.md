# MasterClass WebApi .NET Core

## [0. Prérequis](#chapter-0)
## [1. Initialisation du projet](#chapter-1)
## [2. Action et routage](#chapter-2)
### [2.1 Table de routage](#chapter-2-1)
### [2.2 Ajout d'une route](#chapter-2-2)
## [3. Docker](#chapter-3)
### [3.1 Environnement de debug](#chapter-3-1)
### [3.2 Environnement de production](#chapter-3-2)
## [4. Settings](#chapter-4)
### [4.1 Settings par défaut](#chapter-4-1) 
### [4.2 Settings par environnement](#chapter-4-2)
## [5. Middleware](#chapter-5)
## [6. Injection de dépendances](#chapter-6)
### [6.1 Introduction](#chapter-6-1)
### [6.2 Injection de dépendance dans un middleware](#chapter-6-2)
### [6.3 Mieux comprendre les cycles de vie](#chapter-6-3)
## [7. Logging](#chapter-7)
## [8. Architecture du projet](#chapter-8)
### [8.1 Repository](#chapter-8-1)
### [8.2 Business](#chapter-8-2)
### [8.3 Service](#chapter-8-3)
### [8.4 Dépendances entre les projets](#chapter-8-4)
## [9. Authentification](#chapter-9)
### [9.1 Repository](#chapter-9-1)
### [9.2 Business](#chapter-9-2)
### [9.3 Service](#chapter-9-3)
### [9.4 WebApi](#chapter-9-4)
### [9.5 Résolution par injection de dépendances](#chapter-9-5)
## [10. Json Web Token (JWT)](#chapter-10)
### [10.1 Principe](#chapter-10-1)
### [10.2 Implémentation](#chapter-10-2)
## [11. Tests](#chapter-11)
### [11.1 Initialisation de la solution de test](#chapter-11-1)
### [11.2 Test unitaire](#chapter-11-2)
### [11.3 Test d'intégration](#chapter-11-3)
### [11.4 CI](#chapter-11-4)
## [12. Swagger](#chapter-12)
### [12.1 Documentation](#chapter-12-1)
### [12.2 Authentification JWT](#chapter-12-2)
## [13. Cookie d'authentification](#chapter-13)
## [14. Autorisation](#chapter-14)
### [14.1 Role](#chapter-14-1)
### [14.2 Policy](#chapter-14-2)
### [14.3 Requirement](#chapter-14-3)
### [14.4 Policy Provider](#chapter-14-4)

## 0. Prérequis <a id="chapter-0"></a> 

- VS Code
- Docker et Docker Compose
- Extensions C#, optionnelle Docker
- Fiddler ou Postman

## 1. Initialisation du projet <a id="chapter-1"></a> 

Dans la console VS Code
Création d'un fichier solution
> dotnet new sln -n MasterClass.WebApi

Création d'un répertoire src
> mkdir src  
cd src

Création d'un projet webapi :
> dotnet new webapi -n MasterClass.WebApi

Ajout du projet dans la solution
> cd ..  
dotnet sln add ./src/MasterClass.WebApi/MasterClass.WebApi.csproj

Build de la solution
> dotnet build

Le plugin C# vous propose d'ajouter les fichiers nécessaires pour le debug  
![](doc/images/required_assets.png)


## 2. Action et routage <a id="chapter-2"></a>  

### 2.1 Table de routage <a id="chapter-2-1"></a>  

Il existe différentes manières d'enregistrer nos routes dans la table de routage :
- *MapRoute* permet d'associer un template d'url à une action d'un controller. Les templates sont pris dans l'ordre de déclarations au moment de la résolution de l'URL : le premier template qui correspond donnera l'action de controller à utiliser (les autres seront ignorés).  
- Le routage par attribut. En utilisant l'attribut *Route* sur le controller, vous définissez un préfixe pour toutes les actions du controller. En l'utilisant au niveau de l'action vous complétez le prefix de votre controller. 

### 2.2 Ajout d'une route <a id="chapter-2-2"></a>  

Un test de vie est indispensable afin de permettre par exemple à un loadbalancer de vérifier la bonne santé de l'applicatif. Si le test de vie sur un serveur est KO, le loadbalancer prendra de lui même la décision de sortir la machine du pool

Cela sera un bon exercice afin de créer notre première route  
- Création d'un nouveau controller *DiagnosticController* dans le dossier Controllers
```c#
public class DiagnosticController : Controller
```
- Création de l'action
```c#
public IActionResult HealthCheck() => Ok("system_ok");
```
- Gestion du prefix de routage global au controller
```c#
[Route("api/_system")]
```
- Gestion du routage au niveau de l'action de manière à précier la route, et les méthodes http autorisées
```c#
[HttpGet, HttpHead, Route("healthcheck")]
```

l'url *http://localhost:5000/api/_system/healthcheck* répond alors en GET et en HEAD

## 3.Docker <a id="chapter-3"></a> 

La portabilité de .NET Core nous permet de faire tourner notre application sur un docker. Nous allons automatiser la création de conteneurs à la fois en debug mais aussi en production

### 3.1 Environnement de debug <a id="chapter-3-1"></a> 
Création d'un fichier Dockerfile.debug
> echo. > Dockerfile.debug

```Dockerfile
FROM microsoft/aspnetcore-build:2.0 AS build-env
RUN mkdir /app
WORKDIR /app

COPY ./src/ ./
RUN dotnet restore MasterClass.WebApi/MasterClass.WebApi.csproj
RUN dotnet publish -c Debug MasterClass.WebApi/MasterClass.WebApi.csproj -o out --force

#Install debugger
FROM microsoft/aspnetcore:2.0
ENV NUGET_XMLDOC_MODE skip
WORKDIR /vsdbg
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
       unzip \
    && rm -rf /var/lib/apt/lists/* \
    && curl -sSL https://aka.ms/getvsdbgsh | bash /dev/stdin -v latest -l /vsdbg
WORKDIR /app/MasterClass.WebApi/out
COPY --from=build-env /app/MasterClass.WebApi/out .
# Kick off a container just to wait debugger to attach and run the app
ENTRYPOINT ["/bin/bash", "-c", "sleep infinity"]
```

Afin de profiter du formatage et de l'intellisense de l'extension docker vous pouvez rajouter un fichier de settings
> echo. > ./.vscode/settings.json

```json
{
    "files.associations": {
        "dockerfile.*": "dockerfile"
    }
}
```

Nous utilisons un fichier docker-compose afin de construire l'image et le container docker utilisé dans le debug
> echo. > docker-compose.debug.yml

```yml
version: '2.1'

services:
  masterclass_debug:
    image: masterclass.webapi:debug
    container_name: masterclass-webapi
    build:
      context: .
      dockerfile: Dockerfile.debug
    ports:
      - 5000:80
    environment:
      - ASPNETCORE_ENVIRONMENT=Development
      - DOTNET_USE_POLLING_FILE_WATCHER=1
    entrypoint: tail -f /dev/null 
```

Ajout d'une tache dans le fichier *tasks.json* qui lancera le docker-compose

```json
{
    "label": "buildDocker",
    "command": "docker-compose",
    "type": "process",
    "args": [
        "-f",
        "${workspaceFolder}/docker-compose.debug.yml",
        "up",
        "-d",
        "--build"
    ],
    "problemMatcher": "$msCompile"
}
```

Ajout d'un configuration dans le fichier *launch.json*

```json
{
    "name": ".NET Core Docker Launch (console)",
    "type": "coreclr",
    "request": "launch",
    "preLaunchTask": "buildDocker",
    "program": "/app/MasterClass.WebApi/out/MasterClass.WebApi.dll",
    "cwd": "/app/MasterClass.WebApi/out",
    "sourceFileMap": {
        "/app": "${workspaceRoot}"
    },
    "pipeTransport": {
        "pipeProgram": "docker",
        "pipeCwd": "${workspaceRoot}",
        "pipeArgs": [
            "exec -i masterclass-webapi"
        ],
        "quoteArgs": false,
        "debuggerPath": "/vsdbg/vsdbg"
    }
}
```

Vous pouvez à présent débugguer la solution à la fois dans votre environnement mais aussi dans un docker

### 3.2 Environnement de production <a id="chapter-3-2"></a> 

Comme vous avez pu le constater nous avons construit dans l'étape précédente un conteneur docker sur lesquel est installé en plus du framework aspnet core le debugger visual studio. De plus la solution est publié en mode debug.  
Nous allons créer un configuration de déploiement pour l'environnement de production
> echo. > Dockerfile

```Dockerfile
FROM microsoft/aspnetcore-build:2.0 AS build-env
WORKDIR /app

# copy everything and build
COPY ./src/ ./
RUN dotnet restore MasterClass.WebApi/MasterClass.WebApi.csproj 
RUN dotnet publish MasterClass.WebApi/MasterClass.WebApi.csproj -c Release -o out --force

# build runtime image
FROM microsoft/aspnetcore:2.0
WORKDIR /app/Masterclass.WebApi
COPY --from=build-env /app/MasterClass.WebApi/out .
ENTRYPOINT dotnet MasterClass.WebApi.dll
```

> echo. > docker-compose.yml

```yml
version: '2.1'

services:
  masterclass:
    image: masterclass.webapi
    build:
      context: .
      dockerfile: Dockerfile
    ports:
      - 80:80
    environment:
      - ASPNETCORE_ENVIRONMENT=Production
```

Le conteneur peut être monté en utilisant docker-compose
> docker-compose -f docker-compose.yml up -d --build

Et détruit de la manière suivante
> docker-compose -f docker-compose.yml down

## 4. Settings <a id="chapter-4"></a> 

### 4.1 Settings par défaut <a id="chapter-4-1"></a> 

Un fichier *appsettings.json* est présent dans la solution. Son utilisation est ajouté par défaut dans la construction de notre webhost.
```c#
WebHost.CreateDefaultBuilder(args)
```

Nous allons rajouter un settings afin de rendre configurable le texte renvoyé par notre test de vie.
```json
"Diagnostic": {
  "HealthCheckContent": "system_ok"
}
```

Nous allons mettre nos classes de settings dans un projet Core.
> cd src  
dotnet new classlib -n MasterClass.Core

Ajout du projet dans la solution
> cd ..  
dotnet sln add ./src/MasterClass.Core/MasterClass.Core.csproj

On ajoute la référence de ce projet au projet MasterClass.WebApi
> dotnet add ./src/MasterClass.WebApi/MasterClass.WebApi.csproj reference ./src/MasterClass.Core/MasterClass.Core.csproj

Puis on ajoute la classe *DiagnosticOptions* dans le projet Core
```c#
public class DiagnosticOptions
{
    public string HealthCheckContent { get; set; }
}
```

Nous allons ensuite ajouter un constructeur public à notre *DiagnosticController* et utiliser la propriété *HealthCheckContent* de notre classe de settings
```c#
private readonly DiagnosticOptions _options;

public DiagnosticController(IOptions<DiagnosticOptions> options)
{
    _options = options.Value;
}

[HttpGet, HttpHead, Route("healthcheck")]
public IActionResult HealthCheck() => Ok(_options.HealthCheckContent);
```

Enfin il ne nous reste plus qu'à enregistrer notre classe de settings dans la classe *Startup* afin de pouvoir résoudre la création d'une instance de notre controller via injection de dépendance
```c#
services.Configure<DiagnosticOptions>(Configuration.GetSection("Diagnostic"));
```

Avec cette solution la valeur de notre settings n'est pas prise à chaud : si on modifie le fichier settings il faut redémarrer l'application afin de prendre en compte la modification.

Pour que la valeur du settings soit fixée uniquement sur le cycle de vie de la requête, nous pouvons utiliser *IOptionsSnapshot* (Chaque nouvelle requête récuperera la valeur actuelle du settings et la préservera pendant sa durée de vie)
```c#
public DiagnosticController(IOptionsSnapshot<DiagnosticOptions> options)
```

Pour avoir le changement à chaud, nous pouvons utiliser un *IOptionsMonitor*
```c#
public DiagnosticController(IOptionsMonitor<DiagnosticOptions> options)
{
    _options = options.CurrentValue;
}
```

### 4.2 Settings par environnement <a id="chapter-4-2"></a> 

Lors de la construction de notre webHost un fichier *appsetings.{environment}.json* est également chargé si il est présent.  
*{environment}* est récupéré à partir de la variable d'environnement *ASPNETCORE_ENVIRONMENT*    
Ce fichier ira se greffer en surcharge du fichier *appsettings.json*  

Nous allons créer un fichier de settings pour la production
> echo. > ./src/MasterClass.WebApi/appsettings.Production.json

```json
{
    "Diagnostic": {
        "HealthCheckContent": "prod_ok"
    }
}
```

Dans le fichier *launch.json* nous pouvons modifier la variable d'environnement afin d'utiliser notre settings :  
```json
"env": {
    "ASPNETCORE_ENVIRONMENT": "Production"
},
```

## 5. Middleware <a id="chapter-5"></a>

Un middleware est un composant qui s'inscrit dans un pipeline d’application pour gérer les requêtes et les réponses.  
Chaque composant :
- Choisit de passer la requête au composant suivant dans le pipeline.
- Peut travailler avant et après l’appel du composant suivant dans le pipeline.

Nous allons ajouter un middleware qui va nous permettre de tracker le serveur qui traite la requête dans le header de la réponse (pratique sur une ferme de serveur afin d'identifier un serveur défaillant)

```c#
public class TrackMachineMiddleware
{
    private readonly RequestDelegate _next;

    public TrackMachineMiddleware(RequestDelegate next)
    {
        _next = next;
    }
    
    public async Task InvokeAsync(HttpContext context)
    {
        context.Response.Headers.Add("X-Machine", Environment.MachineName);
        await _next(context);
    }
}
```

L'appel au delegate _next permet d'indiquer que nous faisons appel au composant suivant.
Si nous voulions arrêter le traitement il suffirait de ne pas l'appeler. Il est également possible d'effectuer du traitement après cet appel qui s'exécutera donc après les composants suivants dans le pipe.  
Afin d'utiliser ce middleware, nous l'insérons dans le pipeline applicatif de la manière suivante dans la classe *Startup*
```c#
app.UseMiddleware<TrackMachineMiddleware>();
```

Lorsque nous faisons tourner l'application sur un docker, nous pouvons d'ailleurs constater que le nom du conteneur nous est renvoyé dans le header de la réponse  
![](doc/images/track_machine.png)


## 6. Injection de dépendances <a id="chapter-6"></a>  

### 6.1 Introduction <a id="chapter-6-1"></a>  

L’injection de dépendances est une technique de couplage faible entre des classes et leurs dépendances.  
La création d'une instance sera déporté de l'objet d'où elle sera utilisée. Il existe différents types de cycle de vie de ces instances :

- Transient : Instance créée à la demande
- Scoped : Instance créée à la requête
- Singleton : Instance créée à la première demande puis réutilisée sur toutes les autres

### 6.2 Injection de dépendance dans un middleware <a id="chapter-6-2"></a>  

Afin de comprendre et illustrer ce principe important nous allons créer un nouveau middleware qui s'occupera de renseigner des informations de contexte applicatif dans le header de la réponse (en l'occurence un id lié à la requête pour l'exemple)

Création d'une interface pour le contexte applicatif
```c#
public interface IApplicationContext
{
    Guid Id { get; }
}
```

Création de la classe qui implémente l'interface
```c#
public class McContext : IApplicationContext
{
    public McContext()
    {
        this.Id = Guid.NewGuid();

    }
    public Guid Id { get; }
}
```

Création du middleware
```c#
public class TrackApplicationContextMiddleware
{
    private readonly RequestDelegate _next;

    public TrackApplicationContextMiddleware(RequestDelegate next)
    {
        _next = next;
    }
    
    public async Task InvokeAsync(HttpContext context, IApplicationContext appContext)
    {
        context.Response.Headers.Add("X-Guid", appContext.Id.ToString());
        await _next(context);
    }
}
```

Dans la classe *StartUp* nous ajoutons ce middleware
```c#
app.UseMiddleware<TrackApplicationContextMiddleware>();
```

Et nous choisissons de résoudre notre interface sur un cycle de vie *Scope* de manière à ce que notre Guid soit crée à chaque requête
```c#
services.AddScoped<IApplicationContext, McContext>();
```

Nous voyons alors apparaitre un X-Guid dans le header de notre réponse qui change à chaque nouvelle requête.

### 6.3 Mieux comprendre les cycles de vie <a id="chapter-6-3"></a> 

Amusons nous avec l'injection de dépendance pour mieux comprendre les cycles de vie de nos objets.  
Afin de vérifier que le périmètre scope est bien respecté, ajoutons notre *IApplicationContext* dans le constructeur de notre controlleur et comparons les guid
```c#
[Route("api/_system")]
public class DiagnosticController : Controller
{
    private readonly DiagnosticOptions _options;
    private readonly IApplicationContext _appContext;

    public DiagnosticController(IOptionsMonitor<DiagnosticOptions> options, IApplicationContext appContext)
    {
        _options = options.CurrentValue;
        _appContext = appContext;
    }

    [HttpGet, HttpHead, Route("healthcheck")]
    public IActionResult HealthCheck()
    {
        ControllerContext.HttpContext.Response.Headers.Add("X-Guid2", _appContext.Id.ToString());
        return Ok(_options.HealthCheckContent);
    }
}
```

Pour aller plus loin changez l'enregistrement dans notre conteneur d'injection de dépendances sur un mode *Transient*
```c#
services.AddTransient<IApplicationContext, McContext>();
```
On constate alors que le Guid est différent entre notre controlleur et notre middleware

Changez de nouveau l'enregistrement pour revenir à un périmètre *Scope*.  
Maintenant nous allons changer notre middleware de manière à ce que la résolution se fasse au niveau du constructeur au lieu de la méthode
```c#
public class TrackApplicationContextMiddleware
{
    private readonly RequestDelegate _next;
    private readonly IApplicationContext _appContext;

    public TrackApplicationContextMiddleware(RequestDelegate next, IApplicationContext appContext)
    {
        _next = next;
        _appContext = appContext;
    }
    
    public async Task InvokeAsync(HttpContext context)
    {
        context.Response.Headers.Add("X-Guid", _appContext.Id.ToString());
        await _next(context);
    }
}
```
La résolution de dépendance plante au démarrage de l'appli. Car l'instance du middleware est un singleton.  
```An unhandled exception of type 'System.InvalidOperationException' occurred in Microsoft.AspNetCore.Hosting.dll: 'Cannot resolve scoped service 'MasterClass.Core.Context.IApplicationContext' from root provider.'```  

Il faut donc changer le mode d'enregistrement de notre instance.  
```c#
services.AddSingleton<IApplicationContext, McContext>();
```

Nous constatons que le Guid est alors identique sur l'ensemble de nos requêtes...

## 7. Logging <a id="chapter-7"></a>

Nous allons maintenant voir comment fonctionne le système de logging dans .Net Core.  
Lorsque nous utilisons le *CreateDefaultBuilder* dans notre classe *Program* la configuration de logging par défaut qui est appliqué est la suivante
```c#
.ConfigureLogging((Action<WebHostBuilderContext, ILoggingBuilder>) ((hostingContext, logging) =>
{
    logging.AddConfiguration((IConfiguration) hostingContext.Configuration.GetSection("Logging"));
    logging.AddConsole();
    logging.AddDebug();
}));
```

Il est possible de rajouter un fournisseur de log en appelant la méthode d'extension *ConfigureLogging* sur notre *WebHostBuilder* et d'ajouter un singleton sur une classe qui implémente *ILoggerProvider*

Voici par exemple ce que fait la méthode d'extensions *AddConsole*
```c#
public static ILoggingBuilder AddConsole(this ILoggingBuilder builder)
{
    builder.Services.AddSingleton<ILoggerProvider, ConsoleLoggerProvider>();
    return builder;
}
```

C'est bien de le savoir mais dans les faits le plus pratique est d'écrire dans la sortie standard  
Nous pouvons ensuite récupérer la sortie standard et par exemple la pousser dans un elasticsearch puis dans un kibana (je ne vais pas rentrer dans les détails, voici [un lien intéressant](https://logz.io/blog/docker-logging/) si vous voulez creuser le sujet)

Quoiqu'il en soit nous allons voir comment utiliser la fonctionnalité de logging. Cela fonctionne naturellement via l'injection de dépendance  
Si nous reprenons le middleware précédent, nous décidons d'écrire ces informations dans un log plutot que dans le header de la réponse
```c#
public class TrackApplicationContextMiddleware
{
    private readonly RequestDelegate _next;
    private readonly ILogger<TrackApplicationContextMiddleware> _logger;

    public TrackApplicationContextMiddleware(RequestDelegate next, ILogger<TrackApplicationContextMiddleware> logger)
    {
        _next = next;
        _logger = logger;
    }
    
    public async Task InvokeAsync(HttpContext context, IApplicationContext appContext)
    {
        _logger.LogInformation($"X-Guid : {appContext.Id}");
        await _next(context);
    }
}
```

Si vous ne voyez rien dans votre console, vérifier la config des logs (cela permet de définir le niveau minmum par défaut que l'on veut appliquer pour écrire les logs)
```json
"Console": {
    "LogLevel": {
        "Default": "Information"
    }
}
```

## 8. Architecture du projet <a id="chapter-8"></a>

### 8.1 Repository <a id="chapter-8-1"></a>

Ce projet va contenir :

- les modèles de données du repository
- les interfaces de repository
- les implémentations des interfaces de repository
Création du projet :

> cd src  
dotnet new classlib -n MasterClass.Repository  
cd..  
dotnet sln add src/MasterClass.Repository/MasterClass.Repository.csproj

### 8.2 Business <a id="chapter-8-2"></a>

Ce projet va appeler la couche de repository et appliquer des règles métiers sur les datas

> cd src  
dotnet new classlib -n MasterClass.Business  
cd..  
dotnet sln add src/MasterClass.Business/MasterClass.Business.csproj

### 8.3 Service <a id="chapter-8-3"></a>

Ce projet va contenir les modèles de données de notre API ainsi que les contrats d'échanges.
Il contiendra également les mappers qui permettront de transformer les objets récupérés via la couche Business en objets exposés par notre API

> cd src  
dotnet new classlib -n MasterClass.Service  
cd..  
dotnet sln add src/MasterClass.Service/MasterClass.Service.csproj

### 8.4 Dépendances entre les projets <a id="chapter-8-4"></a>

> dotnet add src/MasterClass.Business/MasterClass.Business.csproj reference src/MasterClass.Repository/MasterClass.Repository.csproj  
dotnet add src/MasterClass.Service/MasterClass.Service.csproj reference src/MasterClass.Business/MasterClass.Business.csproj  
dotnet add src/MasterClass.WebApi/MasterClass.WebApi.csproj reference src/MasterClass.Service/MasterClass.Service.csproj

La dépendance entre nos couches va s'organiser via injection de constructeurs :

- Le constructeur de notre controller va prendre en paramètre une instance de notre objet service
- Le constructeur de notre service va prendre en paramètre une instance de notre objet business
- Le constructeur de notre business va prendre en paramètre une instance de notre Repository  

L'injection de dépendance permettra de créer ces différentes instances


## 9. Authentification <a id="chapter-9"></a>

Notre objectif est de créer une route qui va permettre de valider les informations d'authentification de l'utilisateur
> POST api/user/authenticate

Nous allons pour cela implémenter les différentes couches de notre applicatif

### 9.1 Repository <a id="chapter-9-1"></a>

Nous n'allons pas nous interfacer avec une base de données mais utiliser un mock d'une base d'utilisateur

Ajout d'un fichier de settings pour les mock
> mkdir ./src/MasterClass.WebApi/Mock  
echo. > ./src/MasterClass.WebApi/Mock/users.json

```json
{
    "MockUsers": {
        "Users": [{
            "Id": 1,
            "Login": "login1",
            "Password": "pwd1",
            "Name": "name1",
            "Roles": [
                "SuperAdmin",
                "Admin",
                "User"
            ]
        },
        {
            "Id": 2,
            "Login": "login2",
            "Password": "pwd2",
            "Name": "name2",
            "Roles": [
                "Admin",
                "User"
            ]
        },
        {
            "Id": 3,
            "Login": "login3",
            "Password": "pwd3",
            "Name": "name3",
            "Roles": [
                "User"
            ]
        }]
    }
} 
```

Création du modèle User
> mkdir ./src/MasterClass.Repository/Users  
mkdir ./src/MasterClass.Repository/Users/Models  
echo. > ./src/MasterClass.Repository/Users/Models/User.cs  

```c#
namespace MasterClass.Repository.Users.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string[] Roles { get; set; }
    }
}   
```

Création de l'object Mock

> mkdir ./src/MasterClass.Repository/Users/Mock  
echo. > ./src/MasterClass.Repository/Users/Mock/MockUsers.cs  

```c#
using System.Collections.Generic;
using MasterClass.Repository.Users.Models;

namespace MasterClass.Repository.Users.Mock
{
    public class MockUsers
    {
        public List<User> Users { get; set; }
    }
} 
```

Nous allons installer le package Microsoft.Extensions.Options dans le projet MasterClass.Repository afin d'utiliser la résolution de notre fichier de config

> dotnet add ./src/MasterClass.Repository/MasterClass.Repository.csproj package Microsoft.Extensions.Options  

Nous créons une interface pour notre repository, qui expose une méthode permettant de récupérer un utilisateur via son login

> mkdir ./src/MasterClass.Repository/Users/Contracts  
echo. > ./src/MasterClass.Repository/Users/Contracts/IUserRepository.cs  

```c#
using MasterClass.Repository.Users.Models;

namespace MasterClass.Repository.Users
{
    public interface IUserRepository
    {
         User GetUser(string login);
    }
}
```

Et une implémentation de cette interface qui se base sur notre mock
> echo. > ./src/MasterClass.Repository/Users/Mock/MockUserRepository.cs  

```c#
using MasterClass.Repository.Users.Contracts;
using MasterClass.Repository.Users.Models;
using Microsoft.Extensions.Options;
using System.Linq;

namespace MasterClass.Repository.Users.Mock
{
    public class MockUserRepository : IUserRepository
    {
        private readonly MockUsers _mock;

        public MockUserRepository(IOptions<MockUsers> mock) => _mock = mock.Value;

        public User GetUser(string login) => _mock.Users.SingleOrDefault(user => user.Login == login);
    }
}
```

### 9.2 Business <a id="chapter-9-2"></a>

Notre couche business va s'occuper d'exposer une méthode permettant de savoir si les informations d'authentification sont valides

> mkdir ./src/MasterClass.Business/Users  
mkdir ./src/MasterClass.Business/Users/Contracts  
echo. > ./src/MasterClass.Business/Users/Contracts/IUserBusiness.cs  

```c#
using MasterClass.Repository.Users.Models;

namespace MasterClass.Business.Users.Contracts
 {
     public interface IUserBusiness
    {
        User AuthenticateUser(string login, string password);
    }
 }
```

L'implémentation de notre interface va s'appuyer sur notre couche de repository
> echo. > ./src/MasterClass.Business/Users/UserBusiness.cs  

```c#
using MasterClass.Business.Users.Contracts;
using MasterClass.Repository.Users.Contracts;
using MasterClass.Repository.Users.Models;

namespace MasterClass.Business.Users
{
    public class UserBusiness : IUserBusiness
    {
        private readonly IUserRepository _userRepository;

        public UserBusiness(IUserRepository userRepository) => _userRepository = userRepository;

        public User AuthenticateUser(string login, string password)
        {
            var user = _userRepository.GetUser(login);
            return user != null && user.Password == password ? user : null;
        }
    }
}
```

### 9.3 Service <a id="chapter-9-3"></a>

Notre couche service va s'occuper de formatter le résultat de la couche business  

Création de notre contrat de sortie
> mkdir ./src/MasterClass.Service/Users  
mkdir ./src/MasterClass.Service/Users/Contracts  
mkdir ./src/MasterClass.Service/Users/Contracts/Models    
echo. > ./src/MasterClass.Service/Users/Contracts/Models/AuthenticatedUser.cs
```c#
namespace MasterClass.Service.Users.Contracts.Models
{
    public interface IAuthenticatedUser
    {
        int Id { get; }
    }
}
```

Création de notre modèle en sortie avec une static factory
> mkdir ./src/MasterClass.Service/Users/Models  
echo. > ./src/MasterClass.Service/Users/Models/AuthenticatedUser.cs
```c#
using MasterClass.Repository.Users.Models;
using MasterClass.Service.Users.Contracts.Models;

namespace MasterClass.Service.Users.Models
{
    public class AuthenticatedUser : IAuthenticatedUser
    {
        public int Id { get; }

        private AuthenticatedUser(int id)
        {
            Id = id;
        }

        internal static AuthenticatedUser Create(User user)
            => user == null ? null : new AuthenticatedUser(user.Id);
    }
}
```

Création de notre modèle en entrée
> echo. > ./src/MasterClass.Service/Users/Models/AuthenticateParameters.cs
```c#
 namespace MasterClass.Service.Users.Models
 {
     public class AuthenticateParameters
     {
         public string Login { get; set; }
         public string Password { get; set; }
     }
 }
```

Notre interface de service
> echo. > ./src/MasterClass.Service/Users/Contracts/IUserService.cs  
```c#
using MasterClass.Service.Users.Contracts.Models;
using MasterClass.Service.Users.Models;

namespace MasterClass.Service.Users.Contracts
{
    public interface IUserService
    {
        IAuthenticatedUser Authenticate(AuthenticateParameters authParams);
    }
}
```

L'implémentation de notre interface de service
> echo. > ./src/MasterClass.Service/Users/UserService.cs  
```c#
using MasterClass.Business.Users.Contracts;
using MasterClass.Service.Users.Contracts;
using MasterClass.Service.Users.Contracts.Models;
using MasterClass.Service.Users.Models;

namespace MasterClass.Service.Users
{
    public class UserService : IUserService
    {
        private readonly IUserBusiness _userBusiness;

        public UserService(IUserBusiness userBusiness) => _userBusiness = userBusiness;

        public IAuthenticatedUser Authenticate(AuthenticateParameters authParams)
        {
            return AuthenticatedUser.Create(_userBusiness.AuthenticateUser(authParams.Login, authParams.Password));
        }
    }
}
```

### 9.4 WebApi <a id="chapter-9-4"></a>

Création de notre controller qui va se base sur notre couche de service
> echo. > ./src/MasterClass.WebApi/Controllers/UserController.cs  

```c#
using MasterClass.Service.Users.Contracts;
using MasterClass.Service.Users.Models;
using Microsoft.AspNetCore.Mvc;

namespace MasterClass.WebApi.Controllers
{
    [Route("api/user")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService) => _userService = userService;

        [HttpPost, Route("authenticate")]
        public IActionResult Authenticate([FromBody]AuthenticateParameters authParams)
        {
            var authUser = _userService.Authenticate(authParams);
            return authUser == null ? (IActionResult)Unauthorized() : Ok(authUser);
        }
    }
}
```


### 9.5 Résolution par injection de dépendances <a id="chapter-9-5"></a>

Il ne nous reste plus qu'à enregistrer nos différentes couches dans notre conteneur d'injection de dépendances. Par souci d'organisation nous allons créer une méthode d'extension pour chacune de nos couches.


Création de méthodes d'extension pour enregister notre mock
> mkdir ./src/MasterClass.WebApi/StartupExtensions  
echo. > ./src/MasterClass.WebApi/StartupExtensions/MockExtensions.cs  
```c#
using MasterClass.Repository.Users.Mock;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MasterClass.WebApi.StartupExtensions
{
    public static class MockExtensions
    {
        public static IWebHostBuilder UseMockFiles(this IWebHostBuilder hostBuilder)
        {
            hostBuilder.ConfigureAppConfiguration((builderContext, configBuilder) => 
            {
                configBuilder.AddJsonFile("Mock/users.json");
            });

            return hostBuilder;
        }

        public static IServiceCollection ConfigureMock(this IServiceCollection services, IConfiguration config)
        {
            services.Configure<MockUsers>(config.GetSection(nameof(MockUsers)));

            return services;
        }
    }
}
```

Création d'une méthode d'extension pour enregister notre couche repository
> echo. > ./src/MasterClass.WebApi/StartupExtensions/RepositoryExtensions.cs  

```c#
using MasterClass.Repository.Users.Contracts;
using MasterClass.Repository.Users.Mock;
using Microsoft.Extensions.DependencyInjection;

namespace MasterClass.WebApi.StartupExtensions
{
    public static class RepositoryExtensions
    {
        public static IServiceCollection AddRepository(this IServiceCollection services)
        {
            services.AddSingleton<IUserRepository, MockUserRepository>();

            return services;
        }
    } 
}
```

Création d'une méthode d'extension pour enregister notre couche business
> echo. > ./src/MasterClass.WebApi/StartupExtensions/BusinessExtensions.cs  

```c#
using MasterClass.Business.Users.Contracts;
using MasterClass.Business.Users;
using Microsoft.Extensions.DependencyInjection;

namespace MasterClass.WebApi.StartupExtensions
{
    public static class BusinessExtensions
    {
        public static IServiceCollection AddBusiness(this IServiceCollection services)
        {
            services.AddSingleton<IUserBusiness, UserBusiness>();

            return services;
        }
    } 
}
```

Création d'une méthode d'extension pour enregister notre couche service
> echo. > ./src/MasterClass.WebApi/StartupExtensions/ServiceExtensions.cs  

```c#
using MasterClass.Service.Users.Contracts;
using MasterClass.Service.Users;
using Microsoft.Extensions.DependencyInjection;

namespace MasterClass.WebApi.StartupExtensions
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddService(this IServiceCollection services)
        {
            services.AddSingleton<IUserService, UserService>();

            return services;
        }
    } 
}
```

Dans la classe *Program*  

```c#
public static IWebHost BuildWebHost(string[] args) =>
    WebHost.CreateDefaultBuilder(args)
        .UseStartup<Startup>()
        .UseMockFiles()
        .Build();
```

Dans la classe *Startup*
```c#
services.ConfigureMock(Configuration);

services.AddRepository();
services.AddBusiness();
services.AddService();
```

## 10. Json Web Token (JWT) <a id="chapter-10"></a>

### 10.1 Principe <a id="chapter-10-1"></a>

un token JWT se présente sous la forme {header}.{payload}.{signature}

- Le header est utilisé pour décrire le jeton
- le payload contient les informations
- la signature permet de s'assurer de la fiabilité de ces informations

Le header et le payload sont des objets json encodé en base64.
La signature est un hash de {header}.{payload} à partir d'une clé

Tout client ayant connaissance de la clé est donc capable de vérifier la validité de la signature. De plus le payload étant un json en base64, tout le monde est capable de récupérer son contenu  
La sécurité de cette solution vient du fait que si le header ou le payload sont modifiés la signature n'est plus valide.  
Il est également possible d'utiliser un algorithme de chiffrement asymétrique afin de fournir aux applicatifs une clé publique qui permet seulement de valider la signature et de garder secrète une clé privée qui permet de générer la signature.


L'authentification JWT a plusieurs avantages :
- La légéreté de la solution car aucun token n'est persisté  
- Un gain de performance car le client n'a pas besoin d'appeler le serveur pour récupérer les informations stockées dans le token (par contre attention à ne pas mettre de données sensibles dedans car tout le monde peut les lire)
- Une solution robuste

Par contre l'inconvénient majeure de cette solution est qu'il est impossible de révoquer un token avant la fin de sa validité sans perdre la légéreté de la solution

### 10.2 Implémentation <a id="chapter-10-2"></a>

Nous allons partir sur un algorythme symétrique pour notre API car notre clé de chiffrement n'aura pas à être partagée (la génération et la validation du token seront faits systématiquement au niveau de notre API)

Ajout d'une classe d'option
```c#
public class JwtOptions
{
    public bool Enabled { get; set; }
    public string Issuer { get; set; }
    public string Key { get; set; }
    public TimeSpan Duration { get; set; }
}
```

et d'une entrée dans le *appSettings.json*
```json
"JwtOptions": {
  "Enabled": true,
  "Issuer": "soat.fr",
  "Key": "xa4Z9HqY%}s/v~F>",
  "Duration": "0.00:30:00"
}
```

On crée une méthode d'extension afin d'ajouter le schéma d'authentification et le middleware nécessaire pour valider l'authentification
```c#
public static IServiceCollection AddMasterClassJwt(this IServiceCollection services, IConfiguration config)
{
    var jwtConfigSection = config.GetSection(nameof(JwtOptions));
    services.Configure<JwtOptions>(jwtConfigSection);
    
    var jwtOptions = jwtConfigSection.Get<JwtOptions>();

    if (jwtOptions?.Enabled ?? false)
    {
        services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = jwtOptions.Issuer,
                    IssuerSigningKey = new SymmetricSecurityKey(jwtOptions.Key.ToBytes()),
                    AuthenticationType = JwtBearerDefaults.AuthenticationScheme,
                    ClockSkew = TimeSpan.FromSeconds(0)
                };
            });
    }

    return services;
}
```

On ajoute le package System.IdentityModel.Tokens.Jwt à notre couche de service afin de pouvoir y générer le token  
> dotnet add ./src/MasterClass.Service/MasterClass.Service.csproj package System.IdentityModel.Tokens.Jwt

On crée une méthode d'extension pour gérer la création de nos claims  
```c#
private const string JWTROLE_CLAIMNAME = "role";

public static IEnumerable<Claim> GetJwtClaims(this User user, DateTimeOffset issuedAt)
{
    foreach (var role in user.Roles)
    {
        yield return new Claim(JWTROLE_CLAIMNAME, role);
    }

    yield return new Claim(JwtRegisteredClaimNames.Iat, issuedAt.ToUnixTimeSeconds().ToString());
    yield return new Claim(JwtRegisteredClaimNames.UniqueName, user.Id.ToString());
    yield return new Claim(JwtRegisteredClaimNames.Sub, user.Login);
    yield return new Claim(JwtRegisteredClaimNames.GivenName, user.Name);
    yield return new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString());
}
```

On ajoute un token au modèle exposé en sortie du *authenticate*
```c#
using MasterClass.Repository.Users.Models;
using MasterClass.Service.Users.Contracts.Models;

namespace MasterClass.Service.Users.Models
{
    public class AuthenticatedUser : IAuthenticatedUser
    {
        public int Id { get; }

        public string Token { get; }

        private AuthenticatedUser(int id, string token)
        {
            Id = id;
            Token = token;
        }

        internal static AuthenticatedUser Create(User user, string token)
            => user == null ? null : new AuthenticatedUser(user.Id, token);
    }
}
```

On génère notre token dans notre couche de service en s'appuyant sur notre option (passée dans le constructeur par injection de dépendance)  
```c#
public IAuthenticatedUser Authenticate(AuthenticateParameters authParams, DateTimeOffset issuedAt)
{
    var user = _userBusiness.AuthenticateUser(authParams.Login, authParams.Password);
    if (user != null)
    {
        var jwtToken = _options.Enabled
            ? new JwtSecurityToken(
                issuer: _options.Issuer,
                claims: user.GetJwtClaims(issuedAt),
                expires: issuedAt.LocalDateTime.Add(_options.Duration),
                signingCredentials: new SigningCredentials(new SymmetricSecurityKey(_options.Key.ToBytes()), SecurityAlgorithms.HmacSha256))
            : null;

        return AuthenticatedUser.Create(user, jwtToken == null ? null : new JwtSecurityTokenHandler().WriteToken(jwtToken));
    }
    return null;
}
```
Dans la classe *Startup* on appelle notre méthode d'extensions  
```c#
services.AddMasterClassJwt(Configuration);
```
Et on positionne le middleware d'authentification. Tous les middleware déclarés avant ne pourront pas s'appuyer sur l'authentification mais ceux d'après auront l'information.
```c#
app.UseAuthentication();
```

Enfin on fait en sorte d'avoir une route authentifiée  
```c#
using MasterClass.Service.Users.Contracts;
using MasterClass.Service.Users.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MasterClass.WebApi.Controllers
{
    [Route("api/user"), Authorize]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly ISystemClock _clock;

        public UserController(IUserService userService, ISystemClock clock)
        {
            _userService = userService;
            _clock = clock;
        }

        [HttpGet]
        public IActionResult GetContext() => Ok(new { Id = User.Identity.Name });

        [HttpPost, Route("authenticate"), AllowAnonymous]
        public IActionResult Authenticate([FromBody]AuthenticateParameters authParams)
        {
            var authUser = _userService.Authenticate(authParams, _clock.UtcNow);
            return authUser == null ? (IActionResult)Unauthorized() : Ok(authUser);
        }
    }
}
```

Dans cet exemple nous avons authentifié toutes les routes du controlleur par défaut, et autorisé explicitement la route *authenticate* à ne pas l'être. Nous aurions pu faire l'inverse  
Nous nous basons également sur *ISystemClock* qui permet de récupérer l'heure (et de la mocker éventuellement en cas de test)

## 11. Tests <a id="chapter-11"></a>

### 11.1 Initialisation de la solution de test <a id="chapter-11-1"></a>

Création d'une solution de tests
> mkdir test  
cd test  
dotnet new sln -n MasterClass.WebApi.Test  

Ajout d'un projet de test dans le répertoire test  
> dotnet new xunit -n MasterClass.WebApi.Test  
dotnet sln add ./MasterClass.WebApi.Test/MasterClass.WebApi.Test.csproj  

Nous référençons le projet à tester dans le projet de test
> cd ..  
dotnet add ./test/MasterClass.WebApi.Test/MasterClass.WebApi.Test.csproj reference ./src/MasterClass.WebApi/MasterClass.WebApi.csproj

Afin de bénéficier de l'intellisense de l'extension omnisharp nous le rajoutons dans notre fichier *MasterClass.WebApi.sln*
> dotnet sln add ./test/MasterClass.WebApi.Test/MasterClass.WebApi.Test.csproj  

Ajout du package *Moq* qui nous permettra de mocker nos objets
> dotnet add ./test/MasterClass.WebApi.Test/MasterClass.WebApi.Test.csproj package Moq   

Si vous avez installé l'extension *.NET Core Test Explorer* il faut rajouter cette config dans le fichier *settings.json* (répertoire *.vscode*)  
```json
"dotnet-test-explorer.testProjectPath": "test"
```
### 11.2 Test unitaire <a id="chapter-11-2"></a>

Ajout d'un builder pour générer notre *Fixture* (fixe un environnement pour l'exécution des tests)
> mkdir ./test/MasterClass.WebApi.Test/Controllers  
mkdir ./test/MasterClass.WebApi.Test/Controllers/Fixtures  
echo. > ./test/MasterClass.WebApi.Test/Controllers/Fixtures/UserControllerFixtureBuilder.cs
```c#
using System;
using MasterClass.Service.Users.Contracts;
using MasterClass.Service.Users.Contracts.Models;
using MasterClass.Service.Users.Models;
using MasterClass.WebApi.Controllers;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;
using Moq;

namespace MasterClass.WebApi.Test.Controllers.Fixtures
{
    public class UserControllerFixtureBuilder
    {
        private ServiceCollection _services;
        private readonly Mock<IUserService> _mockUserService;

        public UserControllerFixtureBuilder()
        {
            _services = new ServiceCollection();
            _mockUserService = new Mock<IUserService>();
        }

        public UserControllerFixtureBuilder Initialize()
        {
            _services.Clear();
            
            _services.AddSingleton<IUserService>(_mockUserService.Object);
            _services.AddSingleton<ISystemClock, SystemClock>();
            _services.AddTransient<UserController>(sp => new UserController(sp.GetService<IUserService>(), sp.GetService<ISystemClock>()));

            return this;
        }

        public UserControllerFixtureBuilder AddValidAuthentication(AuthenticateParameters authParams, IAuthenticatedUser user)
        {
            _mockUserService.Setup(userService => userService.Authenticate(authParams, It.IsAny<DateTimeOffset>()))
                .Returns(user);

            return this;
        }

        public UserControllerFixtureBuilder AddInvalidAuthentication(AuthenticateParameters authParams)
        {
            _mockUserService.Setup(userService => userService.Authenticate(authParams, It.IsAny<DateTimeOffset>()))
                .Returns((IAuthenticatedUser)null);
                
            return this;
        }

        public IServiceProvider Build()
        {
            return _services.BuildServiceProvider();
        }
    }
}
```

Ajout d'une classe de test pour tester notre controller *UserController*
> echo. > ./test/MasterClass.WebApi.Test/Controllers/UserController.test.cs  
```c#
using MasterClass.Service.Users.Contracts.Models;
using MasterClass.Service.Users.Models;
using MasterClass.WebApi.Controllers;
using MasterClass.WebApi.Test.Controllers.Fixtures;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Xunit;

namespace MasterClass.WebApi.Test.Controllers
{
    public class UserControllerTest : IClassFixture<UserControllerFixtureBuilder>
    {
        private readonly UserControllerFixtureBuilder _fixtureBuilder;

        public UserControllerTest(UserControllerFixtureBuilder fixtureBuilder)
        {
            _fixtureBuilder = fixtureBuilder;
        }

        #region Authenticate
        [Fact]
        public void Authenticate_Valid()
        {
            //Given
            var authParams = Mock.Of<AuthenticateParameters>();
            var authUser = Mock.Of<IAuthenticatedUser>();

            var userController = _fixtureBuilder
                .Initialize()
                .AddValidAuthentication(authParams, authUser)
                .Build().GetService<UserController>();

            //When
            var actionResult = userController.Authenticate(authParams);

            //Then
            Assert.IsAssignableFrom<OkObjectResult>(actionResult);
            var model = (actionResult as OkObjectResult)?.Value;
            Assert.IsAssignableFrom<IAuthenticatedUser>(model);
            Assert.NotNull(model);
            Assert.Equal(authUser, model);
        }

        [Fact]
        public void Authenticate_Invalid()
        {
            //Given
            var authParams = Mock.Of<AuthenticateParameters>();
            
            var userController = _fixtureBuilder
                .Initialize()
                .AddInvalidAuthentication(authParams)
                .Build().GetService<UserController>();

            //When
            var actionResult = userController.Authenticate(authParams);

            //Then
            Assert.IsAssignableFrom<UnauthorizedResult>(actionResult);
            Assert.NotNull(actionResult);
        }

        #endregion
    }
}
```

Si vous n'avez pas installé l'extension *.NET Core Test Explorer* vous pouvez jouer les tests en utilisant l'intellisense de l'extension Omnisharp (actions possible sur les controlleurs et méthodes de test) ou en ligne de commande  
> dotnet test test  

### 11.3 Test d'intégration <a id="chapter-11-3"></a>

Ajout du package *Microsoft.AspNetCore.Mvc.Testing* qui nous permettra de tester notre API
> dotnet add ./test/MasterClass.WebApi.Test/MasterClass.WebApi.Test.csproj package Microsoft.AspNetCore.Mvc.Testing  

Ajout d'une classe de test afin de vérifier que notre test de vie répond sur la bonne route
> echo. > ./test/MasterClass.WebApi.Test/Controllers/DiagnosticController.test.cs  

```c#
using System.Net;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace MasterClass.WebApi.Test.Controllers
{
    public class DiagnosticControllerTest : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public DiagnosticControllerTest(WebApplicationFactory<Startup> fixture)
        {
            _client = fixture.CreateClient();
        }

        [Fact]
        public async void HealthCheck_Get_Status200()
        {
            var response = await _client.GetAsync("api/_system/healthcheck");

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal("system_ok", await response.Content.ReadAsStringAsync());
        }

        [Fact]
        public async void HealthCheck_Head_Status200()
        {
            var response = await _client.SendAsync(new HttpRequestMessage(HttpMethod.Head, "api/_system/healthcheck"));

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
    }
}
```

### 11.4 CI <a id="chapter-11-4"></a>

Nous allons ajouter une CI sur gitlab. Si vous voulez voir le résultat de la CI il vous faudra un compte sur gitlab et connecter votre repository dessus si il n'y est pas déjà  

Ajouter le fichier
> echo. >.gitlab-ci.yml  
```yml
image: microsoft/dotnet:latest

stages:
  - build
  - test

variables:
  project: "MasterClass.WebApi"

before_script:
  - "dotnet restore"

build:
  stage: build
  variables:
    build_path: "src/$project"
  script:
    - "cd $build_path"
    - "dotnet build"

test:
  stage: test
  variables:
    test_path: "test"
  script:
    - "cd $test_path"
    - "dotnet test"
```

Notre pipeline comprends 2 jobs :
- build : Build notre projet
- test : Lance les tests 

Nous allons maintenant utiliser notre ci pour pousser des images dans dockerhub  

Nous allons éditer notre fichier *Dockerfile* qui nous permet de publier le site en release de manière à inclure les tests  
```dockerfile
FROM microsoft/aspnetcore-build:2.0 AS build-env
WORKDIR /app

# copy everything and restore
COPY ./ ./
RUN dotnet restore

# run tests
WORKDIR /app/test
RUN dotnet test

# publish
WORKDIR /app/src
RUN dotnet restore MasterClass.WebApi/MasterClass.WebApi.csproj 
RUN dotnet publish MasterClass.WebApi/MasterClass.WebApi.csproj -c Release -o out --force

# build runtime image
FROM microsoft/aspnetcore:2.0
WORKDIR /app/Masterclass.WebApi
COPY --from=build-env /app/src/MasterClass.WebApi/out .
ENTRYPOINT dotnet MasterClass.WebApi.dll
```

En utilisant le service Docker in Docker, nous allons pouvoir utiliser docker dans notre ci gitlab.  
Modifions notre fichier *.gitlab-ci.yml*
```yml
image: docker:latest

variables:
  DOCKER_DRIVER: overlay
  DOCKER_IMAGE: geoff32/masterclass.webapi
  CONTAINER_TEST_IMAGE: $DOCKER_IMAGE:$CI_BUILD_REF_NAME

services:
- docker:dind

stages:
- build
- publish

build:
  stage: build
  script:
    - docker build -t $CONTAINER_TEST_IMAGE -f Dockerfile .
    - docker login -u $CI_DOCKERHUB_USR -p $CI_DOCKERHUB_PWD
    - docker push $CONTAINER_TEST_IMAGE
  
publish:
  stage: publish
  script: 
    - docker login -u $CI_DOCKERHUB_USR -p $CI_DOCKERHUB_PWD
    - docker pull $CONTAINER_TEST_IMAGE
    - docker image tag $CONTAINER_TEST_IMAGE $DOCKER_IMAGE
    - docker push $DOCKER_IMAGE
  only:
    - master
```

Le stage build permet de jouer les tests puis publier la branche en cours dans notre dockerhub avec un tag correspondant à la branche.  
Le stage publish permet de récupérer le tag issu de la branche pour le pousser sur le latest. Ce dernier ne s'exécutera que sur la branche master  
Les variables CI_DOCKERHUB_USR et CI_DOCKERHUB_PWD sont définies en variables dans notre CI de manière à ne pas les rendre publique  

Il ne nous reste plus qu'à modifier notre *docker-compose.tml* de manière à monter les conteneurs à partir de l'image latest sur notre dockerhub
```yml
version: '2.1'

services:
  masterclass:
    image: geoff32/masterclass.webapi:latest
    ports:
      - 80:80
    environment:
      - ASPNETCORE_ENVIRONMENT=Production
```

Nous pourrions même pousser plus loin en intégrant du Continous Delivery dans notre CI de manière à publier chaque branche automatiquement sur une url dédiée à chaque commit  

## 12. Swagger <a id="chapter-12"></a>

Swagger va nous permettre d'exposer une documentation de notre API.
Dans le cadre de notre projet nous ne souhaitons pas que cette documentation soit visible dans l'environnement de production (par exemple l'API n'est utilisée que sur des applicatifs développés en interne, pas besoin de documenter à l'extérieur du coup)

### 12.1 Documentation <a id="chapter-12-1"></a>

Nous allons ajouter le package *Swashbuckle.AspNetCore* à notre WebApi
> dotnet add ./src/MasterClass.WebApi/MasterClass.WebApi.csproj package Swashbuckle.AspNetCore

Mise en place d'une configuration pour activer/désactiver swagger
```c#
public class SwaggerOptions
{
    public bool Enabled { get; set; }
}
```

Dans *appsettings.json*
```json
"SwaggerOptions": {
  "Enabled": true
}
```

On le désactive pour la production dans *appsettings.Production.json*
```json
"SwaggerOptions": {
  "Enabled": false
}
```

On crée une méthode d'extension qui va permettre de générer un document décrivant notre API et d'enregistrer notre configuration
```c#
private const string VERSION = "v1";
private const string SWAGGER_DOCNAME = "masterclass.webapi." + VERSION;

public static IServiceCollection AddMasterClassSwagger(this IServiceCollection services, IConfiguration config)
{
    var swaggerSection = config.GetSection(nameof(SwaggerOptions));
    services.Configure<SwaggerOptions>(swaggerSection);

    var options = swaggerSection.Get<SwaggerOptions>();
    if (options.Enabled)
    {
        services.AddSwaggerGen(genOptions
            => genOptions.SwaggerDoc(SWAGGER_DOCNAME,
                new Swagger.Info
                {
                    Title = "MasterClass WebApi",
                    Version = VERSION
                }));
    }

    return services;
}
```

On crée une autre méthode d'extension qui va permettre d'utiliser l'UI de swagger.  
*UseSwagger* permet d'exposer le doc généré ci dessus sous forme d'un json et *UseSwaggerUI* permet d'utiliser l'UI swagger
```c#
public static IApplicationBuilder UseMasterClassSwaggerUI(this IApplicationBuilder app, SwaggerOptions options)
{
    if (options.Enabled)
    {
        app.UseSwagger();

        app.UseSwaggerUI(uiOptions => uiOptions.SwaggerEndpoint($"/swagger/{SWAGGER_DOCNAME}/swagger.json", "MasterClass WebApi"));
    }

    return app;
}
```

Dans la méthode *ConfigureServices* de la classe StartUp
```c#
services.AddMasterClassSwagger(Configuration);
```

Dans la méthode *Configure* nous passons les options par injection de dépendance et ajoutons les middleware
```c#
public void Configure(IApplicationBuilder app, IHostingEnvironment env, IOptions<SwaggerOptions> swaggerOptions)
{
    ...
    app.UseMasterClassSwaggerUI(swaggerOptions.Value);
}
```


### 12.2 Authentification JWT <a id="chapter-12-2"></a>

Nous allons rajouter de l'authentification sur notre swagger afin de pouvoir appeler les méthodes authentifiées
```c#
services.AddSwaggerGen(genOptions =>
    {
        genOptions.SwaggerDoc(SWAGGER_DOCNAME,
            new Swashbuckle.AspNetCore.Swagger.Info
            {
                Title = "MasterClass WebApi",
                Version = VERSION
            });
        genOptions.AddJwtBearerSecurity();
    });
```
```c#
private static void AddJwtBearerSecurity(this SwaggerGenOptions options)
{
    options.AddSecurityDefinition(
        JwtBearerDefaults.AuthenticationScheme,
        new ApiKeyScheme
        {
            Name = "Authorization",
            In = "header"
        });
    options.AddSecurityRequirement(
        new Dictionary<string, IEnumerable<string>>
        {
            { JwtBearerDefaults.AuthenticationScheme, new string[] {} }
        });
}
```

## 13. Cookie d'authentification <a id="chapter-13"></a>

Nous allons gérer un deuxième mode d'authentification dans notre application.

Ajout d'une méthode *SignIn* dans notre couche de service qui va permettre de renvoyer un *ClaimsPrincipal*
```c#
public ClaimsPrincipal SignIn(AuthenticateParameters authParams, string scheme)
{
    var user = _userBusiness.AuthenticateUser(authParams.Login, authParams.Password);
    return user == null ? null : user.GetClaimsPrincipal(scheme);
}
```
```c#
public static ClaimsPrincipal GetClaimsPrincipal(this User user, string scheme)
{
    var identity = new GenericIdentity(user.Id.ToString(), scheme);
    identity.AddClaim(new Claim(ClaimTypes.GivenName, user.Name));
    identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Login));
    return new GenericPrincipal(identity, user.Roles);
}
```

Ajout d'une route *signin* dans UserController qui va permettre de créer le cookie et d'une route *signout* qui va permettre de le supprimer
```c#
[HttpPost("signin"), AllowAnonymous]
public async Task<IActionResult> SignInAsync([FromBody]AuthenticateParameters authParams)
{
    var principal = _userService.SignIn(authParams, CookieAuthenticationDefaults.AuthenticationScheme);
    if (principal != null)
    {
        await HttpContext.SignInAsync(principal, new AuthenticationProperties { IsPersistent = true });
        return Ok();
    }
    return Unauthorized();
}

[HttpPost("signout")]
public async Task<IActionResult> SignOutAsync()
{
    await HttpContext.SignOutAsync();
    return Ok();
}
```

Création d'un classe de settings pour gérer nos cookies
```c#
public class CookieOptions
{
    public bool Enabled { get; set; }
    public string Issuer { get; set; }
    public string Name { get; set; }
    public TimeSpan Duration { get; set; }
}
```

Et configuration de ces settings dans le fichier *appsettings.json*
```json
"CookieOptions": {
  "Enabled": true,
  "Issuer": "localhost",
  "Name": "MasterClass.Cookie",
  "Duration": "0.00:30:00"
}
```

Modifions notre classe d'extensions de manière à n'avoir qu'une méthode publique pour l'ajout de notre système d'authentification.  
Dans .Net Core la policy par défaut pour l'autorisation ne valide que le schéma par défaut (et ne gère donc pas le multi schéma). Afin de ne pas avoir à rajouter l'ensemble de nos schémas au niveau de l'attribut *Authorize* nous allons en profiter pour changer la policy par défaut  
```c#
using MasterClass.Core.Options;
using MasterClass.Core.Tools;
using MasterClass.WebApi.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace MasterClass.WebApi.StartupExtensions
{
    public static class AuthenticationExtensions
    {
        public static IServiceCollection AddMasterClassAuthentication(this IServiceCollection services, IConfiguration config)
        {
            var authBuilder = new AuthorizationPolicyBuilder();
            
            if (services.AddMasterClassJwt(config, JwtBearerDefaults.AuthenticationScheme))
            {
                authBuilder.AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme);
            }

            if (services.AddMasterClassCookie(config, CookieAuthenticationDefaults.AuthenticationScheme))
            {
                authBuilder.AddAuthenticationSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
            }

            var defaultPolicy = authBuilder
                .RequireAuthenticatedUser()
                .Build();

            services.AddAuthorization(options => options.DefaultPolicy = defaultPolicy);

            return services;
        }

        private static bool AddMasterClassCookie(this IServiceCollection services, IConfiguration config, string scheme)
        {
            var cookieOptions = config.GetSection(nameof(CookieOptions)).Get<CookieOptions>();
            if (cookieOptions?.Enabled ?? false)
            {
                services
                    .AddAuthentication(options
                        => options.DefaultSignInScheme
                            = options.DefaultSignOutScheme
                            = scheme)
                    .AddCookie(options =>
                    {
                        options.Cookie.Name = cookieOptions.Name;
                        options.Cookie.Domain = cookieOptions.Issuer;
                        options.EventsType = typeof(WebApiCookieAuthenticationEvents);
                        options.ExpireTimeSpan = cookieOptions.Duration;
                    });

                services.AddScoped<WebApiCookieAuthenticationEvents>();

                return true;
            }

            return false;
        }

        private static bool AddMasterClassJwt(this IServiceCollection services, IConfiguration config, string scheme)
        {
            var jwtConfigSection = config.GetSection(nameof(JwtOptions));
            services.Configure<JwtOptions>(jwtConfigSection);

            var jwtOptions = jwtConfigSection.Get<JwtOptions>();

            if (jwtOptions?.Enabled ?? false)
            {
                services
                    .AddAuthentication(scheme)
                    .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateAudience = false,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = jwtOptions.Issuer,
                            IssuerSigningKey = new SymmetricSecurityKey(jwtOptions.Key.ToBytes()),
                            AuthenticationType = scheme,
                            ClockSkew = TimeSpan.FromSeconds(0)
                        };
                    });

                    return true;
            }

            return false;
        }
    }
}
```

Nous créons une classe *WebApiCookieAuthenticationEvents* afin de surcharger les comportements de redirection standards de l'authent par cookie.  
```c#
public class WebApiCookieAuthenticationEvents : CookieAuthenticationEvents
{
    public override async Task RedirectToAccessDenied(RedirectContext<CookieAuthenticationOptions> context)
    {
        await ReturnStatus(context.Response, StatusCodes.Status403Forbidden);
    }

    public async override Task RedirectToLogin(RedirectContext<CookieAuthenticationOptions> context)
    {
        await ReturnStatus(context.Response, StatusCodes.Status401Unauthorized);
    }

    public async override Task RedirectToLogout(RedirectContext<CookieAuthenticationOptions> context)
    {
        await ReturnStatus(context.Response, StatusCodes.Status401Unauthorized);
    }

    public async override Task RedirectToReturnUrl(RedirectContext<CookieAuthenticationOptions> context)
    {
        await ReturnStatus(context.Response, StatusCodes.Status401Unauthorized);
    }

    private async Task ReturnStatus(HttpResponse response, int status)
    {
        response.StatusCode = status;
        await Task.CompletedTask;
    }
}
```

*AddMasterClassAuthentication* permet :
- De rajouter l'authent JWT si elle est activée  
- De rajouter l'authent cookie si elle est activée  
- De créer un *Policy* par défaut de manière à n'avoir aucun argument à passer à notre attribut *Authorize* (de base il n'y a qu'un seul schéma d'authent par défaut)  

Nous l'appelons dans la classe de *Startup*
```c#
services.AddMasterClassAuthentication(Configuration);
```

## 14. Autorisation <a id="chapter-14"></a>

### 14.1 Role <a id="chapter-14-1"></a>

Il est possible de restreindre l'accès à des actions ou des controlleurs suivants les rôles de l'utilisateur  
```c#
[Route("api/admin"), Authorize, Authorize(Roles = "SuperAdmin, Admin")]
public class AdminController : Controller
{
    [HttpGet]
    public IActionResult Index()
    {
        return Ok();
    }
    
    [HttpGet("grant"), Authorize(Roles = "SuperAdmin")]
    public IActionResult GrantAccess()
    {
        return Ok();
    }
}
```

Dans cet exemple nous cumulons les attributs authorize. Celui sans paramètre s'assure que l'utilisateur est authentifié via la policy par default. Le deuxième vérifie que l'utilisateur a un role suffisant pour y accèder en s'appuyant sur les claims de l'utilisateur authentifié  

### 14.2 Policy <a id="chapter-14-2"></a>

Nous pouvons regrouper un certainement nombre de prérequis sous forme d'une Policy. Nous allons par exemple regrouper la gestion des roles au seins d'une Policy.  
```c#
services.AddAuthorization(options =>
{
    options.DefaultPolicy = defaultPolicy;
    options.AddPolicy(Policies.REQUIRED_SUPERADMIN_ROLE, policy => policy.RequireRole("SuperAdmin"));
    options.AddPolicy(Policies.REQUIRED_ADMIN_ROLE, policy => policy.RequireRole("SuperAdmin, Admin"));
});
```

Notre controlleur devient
```c#
[Route("api/admin"), Authorize, Authorize(Policy = Policies.REQUIRED_ADMIN_ROLE)]
public class AdminController : Controller
{
    [HttpGet]
    public IActionResult Index()
    {
        return Ok();
    }
    
    [HttpGet("grant"), Authorize(Policy = Policies.REQUIRED_SUPERADMIN_ROLE)]
    public IActionResult GrantAccess()
    {
        return Ok();
    }
}
```

Nous allons faire évoluer la définition d'un *SuperAdmin*.  
Tel que cela a été fait jusque maintenant un *SuperAdmin* était un role à part entière.  
A partir de maintenant un *SuperAdmin* est un *Admin* avec un droit de *SuperAdmin*.

Nous allons faire évoluer notre modèle *User* dans notre couche de repository de manière à rajouter cette notion de droit
```c#
public class User
{
    // ...
    public string[] Rights { get; set; }
}
```

Nous ajoutons maintenant l'information dans nos *Claims*
```c#
public static IEnumerable<Claim> GetJwtClaims(this User user, DateTimeOffset issuedAt)
{
    // ...
    foreach (var rightClaim in user.GetMasterClassClaims())
    {
        yield return rightClaim;
    }
    // ...
}

public static ClaimsPrincipal GetClaimsPrincipal(this User user, string scheme)
{
    // ...
    identity.AddClaims(user.GetMasterClassClaims());
    // ...
}

private static IEnumerable<Claim> GetMasterClassClaims(this User user)
{
    return user.Rights == null
        ? Enumerable.Empty<Claim>()
        : user.Rights.Select(right => new Claim(MasterClassClaims.RIGHTS_CLAIMNAME, right));
}
```

Maintenant nous allons faire évoluer notre Policy :
```c#
services.AddAuthorization(options =>
{
    options.DefaultPolicy = defaultPolicy;
    options.AddPolicy(Policies.REQUIRED_SUPERADMIN_ROLE, policy =>
        policy.RequireRole("Admin")
            .RequireClaim(MasterClassClaims.RIGHTS_CLAIMNAME, "SuperAdmin"));
    options.AddPolicy(Policies.REQUIRED_ADMIN_ROLE, policy => policy.RequireRole("Admin"));
});
```

### 14.3 Requirement <a id="chapter-14-3"></a>

Nous pouvons avoir besoin de controle d'accès spécifique par exemple de controller qu'un utilisateur a plus de 18 ans pour pouvoir acheter de l'alcool  
Nous allons utiliser les Requirement dans ce but  
```c#
public class MinimumAgeRequirement : IAuthorizationRequirement
{
    public int MinimumAge { get; }

    public MinimumAgeRequirement(int minimumAge)
    {
        MinimumAge = minimumAge;
    }
}
```

Nous ajoutons une Policy avec ce Requirement  
```c#
options.AddPolicy(Policies.REQUIRED_ATLEAST_18, policy =>
    policy.Requirements.Add(new MinimumAgeRequirement(18)));
```

Nous ajoutons une route qui utilise cette policy
```c#
[Route("api/alcohol"), Authorize, Authorize(Policy = Policies.REQUIRED_ATLEAST_18)]
public class AlcoholController : Controller
{
    [HttpGet]
    public IActionResult Index()
    {
        return Ok();
    }
}
```

Nous ajoutons une date de naissance à notre objet *User*
```c#
public class User
{
    // ...
    public DateTime BirthDate { get; set; }
}
```

Nous ajoutons le claim lors de notre authentification
```c#
private static IEnumerable<Claim> GetMasterClassClaims(this User user)
{
    var rightClaims = user.Rights == null
        ? Enumerable.Empty<Claim>()
        : user.Rights.Select(right => new Claim(MasterClassClaims.RIGHTS_CLAIMNAME, right));

    return rightClaims
        .Concat(new [] { new Claim(ClaimTypes.DateOfBirth, user.BirthDate.ToString() ) });
}
```

Nous rajoutons un handler qui va permettre de valider le Requirement
```c#
public class MinimumAgeHandler : AuthorizationHandler<MinimumAgeRequirement>
{
    protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
                                                MinimumAgeRequirement requirement)
    {
        if (!context.User.HasClaim(c => c.Type == ClaimTypes.DateOfBirth))
        {
            return Task.CompletedTask;
        }

        var dateOfBirth = Convert.ToDateTime(
            context.User.FindFirst(c => c.Type == ClaimTypes.DateOfBirth).Value);

        int calculatedAge = DateTime.Today.Year - dateOfBirth.Year;
        if (dateOfBirth > DateTime.Today.AddYears(-calculatedAge))
        {
            calculatedAge--;
        }

        if (calculatedAge >= requirement.MinimumAge)
        {
            context.Succeed(requirement);
        }

        return Task.CompletedTask;
    }
}
```

Et enfin nous enregistrons ce handler  
```c#
services.AddSingleton<IAuthorizationHandler, MinimumAgeHandler>();
```

### 14.4 Policy Provider <a id="chapter-14-4"></a>

Il peut être intéressant de créer dynamiquement les policy.  
Actuellement le type par défaut utilisé est un *DefaultAuthorizationPolicyProvider* qui se base sur les options fournies dans le *AddAuthorisation*.
Voici ce que fait actuellement le *DefaultAuthorizationPolicyProvider*
```c#
public class DefaultAuthorizationPolicyProvider : IAuthorizationPolicyProvider
{
    private readonly AuthorizationOptions _options;

    public DefaultAuthorizationPolicyProvider(IOptions<AuthorizationOptions> options)
    {
        if (options == null)
        throw new ArgumentNullException(nameof (options));
        this._options = options.Value;
    }

    public Task<AuthorizationPolicy> GetDefaultPolicyAsync()
    {
        return Task.FromResult<AuthorizationPolicy>(this._options.DefaultPolicy);
    }

    public virtual Task<AuthorizationPolicy> GetPolicyAsync(string policyName)
    {
        return Task.FromResult<AuthorizationPolicy>(this._options.GetPolicy(policyName));
    }
}
```

Dans notre cas cela va nous permettre de créer par défaut les schémas utilisés et également de se baser sur une option pour récupérer l'age minimal pour notre controller *Alcohol* (cet age peut varier d'un pays à un autre)

Création d'une factory afin de créer les Policy. Nous nous basons sur le *IAuthenticationSchemeProvider* qui va nous permettre de récupérer tous les schémas enregistrés  
```c#
public class AuthorizationPolicyFactory : IAuthorizationPolicyFactory
{
    private readonly Task<IEnumerable<AuthenticationScheme>> _schemes;
    private readonly AlcoholOptions _alcoholOptions;

    public AuthorizationPolicyFactory(IAuthenticationSchemeProvider schemeProvider, IOptions<AlcoholOptions> alcoholOptions)
    {
        _schemes = schemeProvider.GetAllSchemesAsync();
        _alcoholOptions = alcoholOptions.Value;
    }

    public async Task<AuthorizationPolicy> CreateDefaultAsync()
    {
        return (await CreatePolicyBuilderAsync())
            .RequireAuthenticatedUser()
            .Build();
    }

    public async Task<AuthorizationPolicy> CreateAsync(string policyName)
    {
        switch (policyName)
        {
            case Policies.REQUIRED_SUPERADMIN_ROLE:
                return await CreateSuperAdminPolicyAsync();
            case Policies.REQUIRED_ADMIN_ROLE:
                return await CreateAdminPolicyAsync();
            case Policies.REQUIRED_MINIMUM_AGE_ALCOHOL:
                return await CreateAtLeastPolicyAsync(_alcoholOptions.MinimumAge);
            default:
                throw new ArgumentException($"{policyName} not found", policyName);
        }
    }

    private async Task<AuthorizationPolicy> CreateSuperAdminPolicyAsync()
    {
        var policyBuilder = await CreatePolicyBuilderAsync();
        policyBuilder.RequireRole("Admin")
            .RequireClaim(MasterClassClaims.RIGHTS_CLAIMNAME, "SuperAdmin");
        return policyBuilder.Build();
    }

    private async Task<AuthorizationPolicy> CreateAdminPolicyAsync()
    {
        var policyBuilder = await CreatePolicyBuilderAsync();
        policyBuilder.RequireRole("Admin");
        return policyBuilder.Build();
    }

    private async Task<AuthorizationPolicy> CreateAtLeastPolicyAsync(int minimumAge)
    {
        var policyBuilder = await CreatePolicyBuilderAsync();
        policyBuilder.Requirements.Add(new MinimumAgeRequirement(minimumAge));
        return policyBuilder.Build();
    }

    private async Task<AuthorizationPolicyBuilder> CreatePolicyBuilderAsync()
    {
        return new AuthorizationPolicyBuilder((await GetAllSchemesAsync()).ToArray());
    }

    private async Task<IEnumerable<string>> GetAllSchemesAsync()
    {
        return (await _schemes).Select(scheme => scheme.Name);
    }
}
```

Création de notre provider : nous laissons la possibilité d'utiliser des options comme sur le *DefaultAuthorizationPolicyProvider*  
```c#
public class MasterClassAuthorizationPolicyProvider : IAuthorizationPolicyProvider
{
    private IAuthorizationPolicyFactory _policyFactory;
    private readonly AuthorizationOptions _options;
    private readonly object _lock = new object();

    public MasterClassAuthorizationPolicyProvider(IAuthorizationPolicyFactory policyFactory, IOptions<AuthorizationOptions> options)
    {
        _policyFactory = policyFactory;
        _options = options.Value;
    }

    public async Task<AuthorizationPolicy> GetDefaultPolicyAsync()
    {
        if (_options.DefaultPolicy == null)
        {
            await AddDefaultPolicyAsync();
        }
        return _options.DefaultPolicy;
    }

    public async Task<AuthorizationPolicy> GetPolicyAsync(string policyName)
    {
        var policy = _options.GetPolicy(policyName);
        if (policy == null)
        {
            policy = await AddPolicyAsync(policyName);
        }
        return policy;
    }

    private async Task AddDefaultPolicyAsync()
    {
        var defaultPolicy = await _policyFactory.CreateDefaultAsync();
        lock (_lock)
        {
            if (_options.DefaultPolicy == null)
            {
                _options.DefaultPolicy = defaultPolicy;
            }
        }
    }

    private async Task<AuthorizationPolicy> AddPolicyAsync(string policyName)
    {
        var policy = await _policyFactory.CreateAsync(policyName);
        lock (_lock)
        {
            if (_options.GetPolicy(policyName) == null)
            {
                _options.AddPolicy(policyName, policy);
            }
        }

        return policy;
    }
}
```

On ajoute l'enregistrement de ces 2 classes et de la classe d'options, ainsi que refacto de la méthode *AddMasterClassAuthentication*  
```c#
public static IServiceCollection AddMasterClassAuthentication(this IServiceCollection services, IConfiguration config)
{
    services.AddMasterClassJwt(config);
    services.AddMasterClassCookie(config);

    return services;
}

public static IServiceCollection AddMasterClassPolicy(this IServiceCollection services, IConfiguration config)
{
    services.Configure<AlcoholOptions>(config.GetSection(nameof(AlcoholOptions)));
    services.AddAuthorization(options => options.DefaultPolicy = null);

    services.AddSingleton<IAuthorizationPolicyFactory, AuthorizationPolicyFactory>();
    services.AddTransient<IAuthorizationPolicyProvider, MasterClassAuthorizationPolicyProvider>();
    services.AddSingleton<IAuthorizationHandler, MinimumAgeHandler>();

    return services;
}
```

Dans la classe *Startup*
```c#
services.AddMasterClassAuthentication(Configuration);
services.AddMasterClassPolicy(Configuration);
```