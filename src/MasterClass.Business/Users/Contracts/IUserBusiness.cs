using MasterClass.Repository.Users.Models;

namespace MasterClass.Business.Users.Contracts
 {
     public interface IUserBusiness
    {
        User AuthenticateUser(string login, string password);
    }
 }