﻿using MasterClass.Core.Context;
using MasterClass.Core.Options;
using MasterClass.WebApi.Middleware;
using MasterClass.WebApi.StartupExtensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace MasterClass.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<DiagnosticOptions>(Configuration.GetSection("Diagnostic"));

            services.ConfigureMock(Configuration);

            services.AddMasterClassAuthentication(Configuration);
            services.AddMasterClassPolicy(Configuration);

            services.AddRepository();
            services.AddBusiness();
            services.AddService();
            services.AddScoped<IApplicationContext, McContext>();
            services.AddMvc();

            services.AddMasterClassSwagger(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IOptions<SwaggerOptions> swaggerOptions)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<TrackMachineMiddleware>();
            app.UseAuthentication();
            app.UseMiddleware<TrackApplicationContextMiddleware>();
            
            app.UseMvc();

            app.UseMasterClassSwaggerUI(swaggerOptions.Value);
        }
    }
}
