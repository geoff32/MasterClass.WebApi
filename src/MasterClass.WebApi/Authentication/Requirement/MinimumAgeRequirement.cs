using Microsoft.AspNetCore.Authorization;

namespace MasterClass.WebApi.Authentication.Requirement
{
    public class MinimumAgeRequirement : IAuthorizationRequirement
    {
        public int MinimumAge { get; }

        public MinimumAgeRequirement(int minimumAge)
        {
            MinimumAge = minimumAge;
        }
    }
}