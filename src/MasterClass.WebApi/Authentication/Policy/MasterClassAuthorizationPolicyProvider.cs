using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;

namespace MasterClass.WebApi.Authentication.Policy
{
    public class MasterClassAuthorizationPolicyProvider : IAuthorizationPolicyProvider
    {
        private IAuthorizationPolicyFactory _policyFactory;
        private readonly AuthorizationOptions _options;
        private readonly object _lock = new object();

        public MasterClassAuthorizationPolicyProvider(IAuthorizationPolicyFactory policyFactory, IOptions<AuthorizationOptions> options)
        {
            _policyFactory = policyFactory;
            _options = options.Value;
        }

        public async Task<AuthorizationPolicy> GetDefaultPolicyAsync()
        {
            if (_options.DefaultPolicy == null)
            {
                await AddDefaultPolicyAsync();
            }
            return _options.DefaultPolicy;
        }

        public async Task<AuthorizationPolicy> GetPolicyAsync(string policyName)
        {
            var policy = _options.GetPolicy(policyName);
            if (policy == null)
            {
                policy = await AddPolicyAsync(policyName);
            }
            return policy;
        }

        private async Task AddDefaultPolicyAsync()
        {
            var defaultPolicy = await _policyFactory.CreateDefaultAsync();
            lock (_lock)
            {
                if (_options.DefaultPolicy == null)
                {
                    _options.DefaultPolicy = defaultPolicy;
                }
            }
        }

        private async Task<AuthorizationPolicy> AddPolicyAsync(string policyName)
        {
            var policy = await _policyFactory.CreateAsync(policyName);
            lock (_lock)
            {
                if (_options.GetPolicy(policyName) == null)
                {
                    _options.AddPolicy(policyName, policy);
                }
            }

            return policy;
        }
    }
}