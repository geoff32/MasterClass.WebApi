using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace MasterClass.WebApi.Authentication.Policy
{
    public interface IAuthorizationPolicyFactory
    {
        Task<AuthorizationPolicy> CreateDefaultAsync();
        Task<AuthorizationPolicy> CreateAsync(string policyName);
    }
}