using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterClass.Core.Options;
using MasterClass.Service.Users.Core;
using MasterClass.WebApi.Authentication.Requirement;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;

namespace MasterClass.WebApi.Authentication.Policy
{
    public class AuthorizationPolicyFactory : IAuthorizationPolicyFactory
    {
        private readonly Task<IEnumerable<AuthenticationScheme>> _schemes;
        private readonly AlcoholOptions _alcoholOptions;

        public AuthorizationPolicyFactory(IAuthenticationSchemeProvider schemeProvider, IOptions<AlcoholOptions> alcoholOptions)
        {
            _schemes = schemeProvider.GetAllSchemesAsync();
            _alcoholOptions = alcoholOptions.Value;
        }

        public async Task<AuthorizationPolicy> CreateDefaultAsync()
        {
            return (await CreatePolicyBuilderAsync())
                .RequireAuthenticatedUser()
                .Build();
        }

        public async Task<AuthorizationPolicy> CreateAsync(string policyName)
        {
            switch (policyName)
            {
                case Policies.REQUIRED_SUPERADMIN_ROLE:
                    return await CreateSuperAdminPolicyAsync();
                case Policies.REQUIRED_ADMIN_ROLE:
                    return await CreateAdminPolicyAsync();
                case Policies.REQUIRED_MINIMUM_AGE_ALCOHOL:
                    return await CreateAtLeastPolicyAsync(_alcoholOptions.MinimumAge);
                default:
                    throw new ArgumentException($"{policyName} not found", policyName);
            }
        }

        private async Task<AuthorizationPolicy> CreateSuperAdminPolicyAsync()
        {
            var policyBuilder = await CreatePolicyBuilderAsync();
            policyBuilder.RequireRole("Admin")
                .RequireClaim(MasterClassClaims.RIGHTS_CLAIMNAME, "SuperAdmin");
            return policyBuilder.Build();
        }

        private async Task<AuthorizationPolicy> CreateAdminPolicyAsync()
        {
            var policyBuilder = await CreatePolicyBuilderAsync();
            policyBuilder.RequireRole("Admin");
            return policyBuilder.Build();
        }

        private async Task<AuthorizationPolicy> CreateAtLeastPolicyAsync(int minimumAge)
        {
            var policyBuilder = await CreatePolicyBuilderAsync();
            policyBuilder.Requirements.Add(new MinimumAgeRequirement(minimumAge));
            return policyBuilder.Build();
        }

        private async Task<AuthorizationPolicyBuilder> CreatePolicyBuilderAsync()
        {
            return new AuthorizationPolicyBuilder((await GetAllSchemesAsync()).ToArray());
        }

        private async Task<IEnumerable<string>> GetAllSchemesAsync()
        {
            return (await _schemes).Select(scheme => scheme.Name);
        }
    }
}