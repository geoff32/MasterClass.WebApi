namespace MasterClass.WebApi.Authentication.Policy
{
    public static class Policies
    {
        public const string REQUIRED_SUPERADMIN_ROLE = "RequiredSuperAdminRole";
        public const string REQUIRED_ADMIN_ROLE = "RequiredAdminRole";
        public const string REQUIRED_MINIMUM_AGE_ALCOHOL = "RequiredMinimumAgeAlcohol";
    }
}