using System.Threading.Tasks;
using MasterClass.Core.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace MasterClass.WebApi.Middleware
{
    public class TrackApplicationContextMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<TrackApplicationContextMiddleware> _logger;

        public TrackApplicationContextMiddleware(RequestDelegate next, ILogger<TrackApplicationContextMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }
        
        public async Task InvokeAsync(HttpContext context, IApplicationContext appContext)
        {
            _logger.LogInformation($"X-Guid : {appContext.Id}");
            await _next(context);
        }
    }
}