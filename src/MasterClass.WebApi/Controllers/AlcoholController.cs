using MasterClass.WebApi.Authentication.Policy;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MasterClass.WebApi.Controllers
{
    [Route("api/alcohol"), Authorize(Policy = Policies.REQUIRED_MINIMUM_AGE_ALCOHOL)]
    public class AlcoholController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return Ok();
        }
    }
}