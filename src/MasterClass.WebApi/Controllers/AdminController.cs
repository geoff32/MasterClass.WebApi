using MasterClass.WebApi.Authentication.Policy;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MasterClass.WebApi.Controllers
{
    [Route("api/admin"), Authorize(Policy = Policies.REQUIRED_ADMIN_ROLE)]
    public class AdminController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return Ok();
        }
        
        [HttpGet("grant"), Authorize(Policy = Policies.REQUIRED_SUPERADMIN_ROLE)]
        public IActionResult GrantAccess()
        {
            return Ok();
        }
    }
}