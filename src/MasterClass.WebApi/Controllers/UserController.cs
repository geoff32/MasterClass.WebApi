using System.Threading.Tasks;
using MasterClass.Core.Options;
using MasterClass.Service.Users.Contracts;
using MasterClass.Service.Users.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace MasterClass.WebApi.Controllers
{
    [Route("api/user"), Authorize]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly ISystemClock _clock;

        public UserController(IUserService userService, ISystemClock clock)
        {
            _userService = userService;
            _clock = clock;
        }

        [HttpGet]
        public IActionResult GetContext() => Ok(new { Id = User.Identity.Name });

        [HttpPost("authenticate"), AllowAnonymous]
        public IActionResult Authenticate([FromBody]AuthenticateParameters authParams)
        {
            var authUser = _userService.Authenticate(authParams, _clock.UtcNow);
            return authUser == null ? (IActionResult)Unauthorized() : Ok(authUser);
        }

        [HttpPost("signin"), AllowAnonymous]
        public async Task<IActionResult> SignInAsync([FromBody]AuthenticateParameters authParams)
        {
            var principal = _userService.SignIn(authParams, CookieAuthenticationDefaults.AuthenticationScheme);
            if (principal != null)
            {
                await HttpContext.SignInAsync(principal, new AuthenticationProperties { IsPersistent = true });
                return Ok();
            }
            return Unauthorized();
        }

        [HttpPost("signout")]
        public async Task<IActionResult> SignOutAsync()
        {
            await HttpContext.SignOutAsync();
            return Ok();
        }
    }
}