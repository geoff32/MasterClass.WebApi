using MasterClass.Core.Options;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace MasterClass.WebApi.Controllers
{
    [Route("api/_system")]
    public class DiagnosticController : Controller
    {
        private readonly DiagnosticOptions _options;

        public DiagnosticController(IOptionsMonitor<DiagnosticOptions> options)
        {
            _options = options.CurrentValue;
        }

        [HttpGet, HttpHead, Route("healthcheck")]
        public IActionResult HealthCheck() => Ok(_options.HealthCheckContent);
    }
}