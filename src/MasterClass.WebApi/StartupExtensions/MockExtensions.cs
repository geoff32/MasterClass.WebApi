using MasterClass.Repository.Users.Mock;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MasterClass.WebApi.StartupExtensions
{
    public static class MockExtensions
    {
        public static IWebHostBuilder UseMockFiles(this IWebHostBuilder hostBuilder)
        {
            hostBuilder.ConfigureAppConfiguration((builderContext, configBuilder) => 
            {
                configBuilder.AddJsonFile("Mock/users.json");
            });

            return hostBuilder;
        }

        public static IServiceCollection ConfigureMock(this IServiceCollection services, IConfiguration config)
        {
            services.Configure<MockUsers>(config.GetSection(nameof(MockUsers)));

            return services;
        }
    }
}