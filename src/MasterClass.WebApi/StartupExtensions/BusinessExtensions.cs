using MasterClass.Business.Users.Contracts;
using MasterClass.Business.Users;
using Microsoft.Extensions.DependencyInjection;

namespace MasterClass.WebApi.StartupExtensions
{
    public static class BusinessExtensions
    {
        public static IServiceCollection AddBusiness(this IServiceCollection services)
        {
            services.AddSingleton<IUserBusiness, UserBusiness>();

            return services;
        }
    } 
}