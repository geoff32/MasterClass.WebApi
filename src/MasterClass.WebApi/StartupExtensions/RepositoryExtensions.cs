using MasterClass.Repository.Users.Contracts;
using MasterClass.Repository.Users.Mock;
using Microsoft.Extensions.DependencyInjection;

namespace MasterClass.WebApi.StartupExtensions
{
    public static class RepositoryExtensions
    {
        public static IServiceCollection AddRepository(this IServiceCollection services)
        {
            services.AddSingleton<IUserRepository, MockUserRepository>();

            return services;
        }
    } 
}