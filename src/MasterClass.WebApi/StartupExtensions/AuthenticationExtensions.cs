using System;
using MasterClass.Core.Options;
using MasterClass.Core.Tools;
using MasterClass.WebApi.Authentication;
using MasterClass.WebApi.Authentication.Policy;
using MasterClass.WebApi.Authentication.Requirement.Handler;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace MasterClass.WebApi.StartupExtensions
{
    public static class AuthenticationExtensions
    {
        public static IServiceCollection AddMasterClassAuthentication(this IServiceCollection services, IConfiguration config)
        {
            services.AddMasterClassJwt(config);
            services.AddMasterClassCookie(config);

            return services;
        }

        public static IServiceCollection AddMasterClassPolicy(this IServiceCollection services, IConfiguration config)
        {
            services.Configure<AlcoholOptions>(config.GetSection(nameof(AlcoholOptions)));
            services.AddAuthorization(options => options.DefaultPolicy = null);

            services.AddSingleton<IAuthorizationPolicyFactory, AuthorizationPolicyFactory>();
            services.AddTransient<IAuthorizationPolicyProvider, MasterClassAuthorizationPolicyProvider>();
            services.AddSingleton<IAuthorizationHandler, MinimumAgeHandler>();

            return services;
        }

        private static IServiceCollection AddMasterClassCookie(this IServiceCollection services, IConfiguration config)
        {
            var cookieOptions = config.GetSection(nameof(CookieOptions)).Get<CookieOptions>();
            if (cookieOptions?.Enabled ?? false)
            {
                services
                    .AddAuthentication(options
                        => options.DefaultSignInScheme
                            = options.DefaultSignOutScheme
                            = CookieAuthenticationDefaults.AuthenticationScheme)
                    .AddCookie(options =>
                    {
                        options.Cookie.Name = cookieOptions.Name;
                        options.Cookie.Domain = cookieOptions.Issuer;
                        options.EventsType = typeof(WebApiCookieAuthenticationEvents);
                        options.ExpireTimeSpan = cookieOptions.Duration;
                    });

                services.AddScoped<WebApiCookieAuthenticationEvents>();
            }

            return services;
        }

        private static IServiceCollection AddMasterClassJwt(this IServiceCollection services, IConfiguration config)
        {
            var jwtConfigSection = config.GetSection(nameof(JwtOptions));
            services.Configure<JwtOptions>(jwtConfigSection);

            var jwtOptions = jwtConfigSection.Get<JwtOptions>();

            if (jwtOptions?.Enabled ?? false)
            {
                services
                    .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateAudience = false,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = jwtOptions.Issuer,
                            IssuerSigningKey = new SymmetricSecurityKey(jwtOptions.Key.ToBytes()),
                            AuthenticationType = JwtBearerDefaults.AuthenticationScheme,
                            ClockSkew = TimeSpan.FromSeconds(0)
                        };
                    });
            }

            return services;
        }
    }
}