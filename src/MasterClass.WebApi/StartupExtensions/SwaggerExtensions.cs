using System.Collections.Generic;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using Options = MasterClass.Core.Options;

namespace MasterClass.WebApi.StartupExtensions
{
    public static class SwaggerExtensions
    {
        private const string VERSION = "v1";
        private const string SWAGGER_DOCNAME = "masterclass.webapi." + VERSION;

        public static IServiceCollection AddMasterClassSwagger(this IServiceCollection services, IConfiguration config)
        {
            var swaggerSection = config.GetSection(nameof(Options.SwaggerOptions));
            services.Configure<Options.SwaggerOptions>(swaggerSection);

            var options = swaggerSection.Get<Options.SwaggerOptions>();
            if (options.Enabled)
            {
                services.AddSwaggerGen(genOptions =>
                    {
                        genOptions.SwaggerDoc(SWAGGER_DOCNAME,
                            new Swashbuckle.AspNetCore.Swagger.Info
                            {
                                Title = "MasterClass WebApi",
                                Version = VERSION
                            });
                        genOptions.AddJwtBearerSecurity();
                    });
            }

            return services;
        }

        public static IApplicationBuilder UseMasterClassSwaggerUI(this IApplicationBuilder app, Options.SwaggerOptions options)
        {
            if (options.Enabled)
            {
                app.UseSwagger();

                app.UseSwaggerUI(uiOptions => uiOptions.SwaggerEndpoint($"/swagger/{SWAGGER_DOCNAME}/swagger.json", "MasterClass WebApi"));
            }

            return app;
        }

        private static void AddJwtBearerSecurity(this SwaggerGenOptions options)
        {
            options.AddSecurityDefinition(
                JwtBearerDefaults.AuthenticationScheme,
                new ApiKeyScheme
                {
                    Name = "Authorization",
                    In = "header"
                });
            options.AddSecurityRequirement(
                new Dictionary<string, IEnumerable<string>>
                {
                    { JwtBearerDefaults.AuthenticationScheme, new string[] {} }
                });
        }
    }
}