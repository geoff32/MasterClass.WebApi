namespace MasterClass.Core.Options
{
    public class AlcoholOptions
    {
        public int MinimumAge { get; set; }
    }
}