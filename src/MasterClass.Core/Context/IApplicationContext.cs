using System;

namespace MasterClass.Core.Context
{
    public interface IApplicationContext
    {
        Guid Id { get; }
    }
}