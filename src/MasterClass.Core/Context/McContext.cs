using System;

namespace MasterClass.Core.Context
{
    public class McContext : IApplicationContext
    {
        public McContext()
        {
            this.Id = Guid.NewGuid();

        }
        public Guid Id { get; }
    }
}