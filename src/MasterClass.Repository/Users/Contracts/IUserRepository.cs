using MasterClass.Repository.Users.Models;

namespace MasterClass.Repository.Users.Contracts
{
    public interface IUserRepository
    {
         User GetUser(string login);
    }
}