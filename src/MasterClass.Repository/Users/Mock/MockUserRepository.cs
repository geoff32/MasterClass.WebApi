using MasterClass.Repository.Users.Contracts;
using MasterClass.Repository.Users.Models;
using Microsoft.Extensions.Options;
using System.Linq;

namespace MasterClass.Repository.Users.Mock
{
    public class MockUserRepository : IUserRepository
    {
        private readonly MockUsers _mock;

        public MockUserRepository(IOptions<MockUsers> mock) => _mock = mock.Value;

        public User GetUser(string login) => _mock.Users.SingleOrDefault(user => user.Login == login);
    }
}