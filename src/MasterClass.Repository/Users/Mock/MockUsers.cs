using System.Collections.Generic;
using MasterClass.Repository.Users.Models;

namespace MasterClass.Repository.Users.Mock
{
    public class MockUsers
    {
        public List<User> Users { get; set; }
    }
} 
