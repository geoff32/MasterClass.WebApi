namespace MasterClass.Service.Users.Contracts.Models
{
    public interface IAuthenticatedUser
    {
        int Id { get; }

        string Token { get; }
    }
}