using System;
using System.Security.Claims;
using MasterClass.Service.Users.Contracts.Models;
using MasterClass.Service.Users.Models;

namespace MasterClass.Service.Users.Contracts
{
    public interface IUserService
    {
        IAuthenticatedUser Authenticate(AuthenticateParameters authParams, DateTimeOffset issuedAt);
        ClaimsPrincipal SignIn(AuthenticateParameters authParams, string scheme);
    }
}