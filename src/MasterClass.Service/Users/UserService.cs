using System;
using System.IdentityModel.Tokens.Jwt;
using MasterClass.Business.Users.Contracts;
using MasterClass.Service.Users.Contracts;
using MasterClass.Service.Users.Models;
using MasterClass.Core.Options;
using MasterClass.Core.Tools;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using MasterClass.Service.Users.Contracts.Models;

namespace MasterClass.Service.Users
{
    public class UserService : IUserService
    {
        private readonly IUserBusiness _userBusiness;
        private readonly JwtOptions _options;

        public UserService(IUserBusiness userBusiness, IOptions<JwtOptions> options)
        {
            _userBusiness = userBusiness;
            _options = options.Value;
        }

        public IAuthenticatedUser Authenticate(AuthenticateParameters authParams, DateTimeOffset issuedAt)
        {
            var user = _userBusiness.AuthenticateUser(authParams.Login, authParams.Password);
            if (user != null)
            {
                var jwtToken = _options.Enabled
                    ? new JwtSecurityToken(
                        issuer: _options.Issuer,
                        claims: user.GetJwtClaims(issuedAt),
                        expires: issuedAt.LocalDateTime.Add(_options.Duration),
                        signingCredentials: new SigningCredentials(new SymmetricSecurityKey(_options.Key.ToBytes()), SecurityAlgorithms.HmacSha256))
                    : null;

                return AuthenticatedUser.Create(user, jwtToken == null ? null : new JwtSecurityTokenHandler().WriteToken(jwtToken));
            }
            return null;
        }

        public ClaimsPrincipal SignIn(AuthenticateParameters authParams, string scheme)
        {
            var user = _userBusiness.AuthenticateUser(authParams.Login, authParams.Password);
            return user == null ? null : user.GetClaimsPrincipal(scheme);
        }
    }
}