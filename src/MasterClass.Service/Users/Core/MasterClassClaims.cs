using System.IO;

namespace MasterClass.Service.Users.Core
{
    public static class MasterClassClaims
    {
        public const string JWTROLE_CLAIMNAME = "role";
        public const string RIGHTS_CLAIMNAME = "rights";
    }
}