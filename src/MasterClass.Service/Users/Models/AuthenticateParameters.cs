 namespace MasterClass.Service.Users.Models
 {
     public class AuthenticateParameters
     {
         public string Login { get; set; }
         public string Password { get; set; }
     }
 } 
