using System.Net;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace MasterClass.WebApi.Test.Controllers
{
    public class DiagnosticControllerTest : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public DiagnosticControllerTest(WebApplicationFactory<Startup> fixture)
        {
            _client = fixture.CreateClient();
        }

        [Fact]
        public async void HealthCheck_Get_Status200()
        {
            var response = await _client.GetAsync("api/_system/healthcheck");

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal("system_ok", await response.Content.ReadAsStringAsync());
        }

        [Fact]
        public async void HealthCheck_Head_Status200()
        {
            var response = await _client.SendAsync(new HttpRequestMessage(HttpMethod.Head, "api/_system/healthcheck"));

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
    }
}