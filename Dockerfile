FROM microsoft/aspnetcore-build:2.0 AS build-env
WORKDIR /app

# copy everything and restore
COPY ./ ./
RUN dotnet restore

# run tests
WORKDIR /app/test
RUN dotnet test

# publish
WORKDIR /app/src
RUN dotnet restore MasterClass.WebApi/MasterClass.WebApi.csproj 
RUN dotnet publish MasterClass.WebApi/MasterClass.WebApi.csproj -c Release -o out --force

# build runtime image
FROM microsoft/aspnetcore:2.0
WORKDIR /app/Masterclass.WebApi
COPY --from=build-env /app/src/MasterClass.WebApi/out .
ENTRYPOINT dotnet MasterClass.WebApi.dll
